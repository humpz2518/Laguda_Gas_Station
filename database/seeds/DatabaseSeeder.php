<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('gasoline_inventory')->insert([
            'id' => 1,
            'gasoline_type' => 'Diesel',
            'price_per_litre' => 0,
            'previous_balance' => 0,
            'current_balance' => 0,
            'created_at' => Carbon::now('Asia/Manila'),
            'updated_at' => Carbon::now('Asia/Manila')
        ]);

        DB::table('gasoline_inventory')->insert([
            'id' => 2,
            'gasoline_type' => 'Premium',
            'price_per_litre' => 0,
            'previous_balance' => 0,
            'current_balance' => 0,
            'created_at' => Carbon::now('Asia/Manila'),
            'updated_at' => Carbon::now('Asia/Manila')
        ]);

        DB::table('gasoline_inventory')->insert([
            'id' => 3,
            'gasoline_type' => 'Unleaded',
            'price_per_litre' => 0,
            'previous_balance' => 0,
            'current_balance' => 0,
            'created_at' => Carbon::now('Asia/Manila'),
            'updated_at' => Carbon::now('Asia/Manila')
        ]);

        DB::table('gasoline_inventory')->insert([
            'id' => 4,
            'gasoline_type' => 'Kerosene',
            'price_per_litre' => 0,
            'previous_balance' => 0,
            'current_balance' => 0,
            'created_at' => Carbon::now('Asia/Manila'),
            'updated_at' => Carbon::now('Asia/Manila')
        ]);

        DB::table('customers')->insert([
            'id' => 1,
            'name' => 'Yoyong Laguda',
            'created_at' => Carbon::now('Asia/Manila'),
            'updated_at' => Carbon::now('Asia/Manila')
        ]);
    }
}
