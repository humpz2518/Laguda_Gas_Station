<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGasolineInventoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gasoline_inventory', function (Blueprint $table) {
            $table->increments('id');
            $table->string('gasoline_type');
            $table->decimal('price_per_litre', 8, 2);
            $table->decimal('previous_balance', 8, 2);
            $table->decimal('current_balance', 8, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gasoline_inventory');
    }
}
