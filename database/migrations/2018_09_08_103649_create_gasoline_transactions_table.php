<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGasolineTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gasoline_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('transaction');
            $table->string('gasoline_type');
            $table->decimal('quantity_in_litre', 8, 2);
            $table->decimal('previous_balance', 8, 2);
            $table->decimal('new_balance', 8, 2);
            $table->string('user');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gasoline_transactions');
    }
}
