<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GasolineTransaction extends Model
{
    protected $table = 'gasoline_transactions';
}
