<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GasolineInventory extends Model
{
    protected $table = 'gasoline_inventory';
}
