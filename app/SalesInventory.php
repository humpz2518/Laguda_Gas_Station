<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesInventory extends Model
{
    protected $table = 'sales_inventory';
}
