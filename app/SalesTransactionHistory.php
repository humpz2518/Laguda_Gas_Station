<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesTransactionHistory extends Model
{
    protected $table = 'sales_transactions';
}
