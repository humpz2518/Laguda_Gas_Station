<?php

namespace App\Http\Controllers;

use App\SalesInventory;
use App\SalesTransactionHistory;
use App\User;
use App\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use App\AccountsReceivable;
use App\Customer;

class SalesController extends Controller
{
    //Add Item
    public function sales_add_item(Request $request)
    {
//        $userid = Auth::user()->id;
        $id = Auth::user()->name;
        $sales = new SalesInventory;
        $sales->product_name = $request->product;
        $history = new SalesTransactionHistory();
        $history->product_name = $request->product;
        $history->transaction = "Add New Item";
        $history->quantity = $request->quantity;
        $history->previous_balance = 0;
        $history->current_balance = $request->quantity;
        $history->user = $id;
        $sales->price = $request->price;
        $sales->previous_balance = 0;
        $sales->current_balance = $request->quantity;
        $sales->save();
        $history->save();
        $log = new Log();
        $log->category = 'Sales Inventory';
        $log->action = 'Added New Item: '.$request->product.' - QTY:'.$request->quantity;
        $log->user = Auth::user()->name;
        $log->created_at = Carbon::now('Asia/Manila');
        $log->updated_at = Carbon::now('Asia/Manila');
        $log->save();
        $e = [
            'type' => 200,
            'body' => 'Successful',
            'message' => 'Successfully Added New Item'
        ];
        $http_response = $e;
        return json_encode($http_response);

    }

    public function save_sales_price(Request $request){
        $sales = SalesInventory::where('product_name',$request->product)->first();
        $e = [
            'type' => 200,
            'body' => 'Successful',
            'message' => 'Failed to Submit'
        ];
        $http_response = $e;
        if(isset($sales)){
            if($sales->price != $request->price){
                $log = new Log();
                $log->category = 'Sales Inventory';
                $log->action = 'Updated Price Of '.$request->product.' From <span>&#8369;</span>'.$sales->price.' To <span>&#8369;</span>'.$request->price;
                $log->user = Auth::user()->name;
                $log->created_at = Carbon::now('Asia/Manila');
                $log->updated_at = Carbon::now('Asia/Manila');
                $log->save();

                $sales->price = $request->price;
                $sales->save();

                $e = [
                    'type' => 200,
                    'body' => 'Successful',
                    'price' => $sales->price,
                    'message' => 'Successfully Updated Price For '. $request->product
                ];
            }
            else{
                $e = [
                    'type' => 200,
                    'body' => 'Successful',
                    'price' => $sales->price,
                    'message' => 'Price Was Not Changed'
                ];
            }
            $http_response = $e;
        }
        return json_encode($http_response);
    }

    public function sales_add_quantity(Request $request){
        $sales = SalesInventory::where('product_name',$request->product)->first();
        $id = Auth::user()->name;
        $e = [
            'type' => 200,
            'body' => 'Successful',
            'message' => 'Failed to Submit'
        ];
        $http_response = $e;
        if(isset($sales)){
            if($request->quantity != 0){
                $log = new Log();
                $log->category = 'Sales Inventory';
                $log->action = 'Added Quantity of '.$request->product.' - QTY:'.$request->quantity;
                $log->user = Auth::user()->name;
                $log->created_at = Carbon::now('Asia/Manila');
                $log->updated_at = Carbon::now('Asia/Manila');
                $log->save();


                $total = $sales->current_balance + $request->quantity;
                $history = new SalesTransactionHistory();
                $history->product_name = $request->product;
                $history->transaction = "Add Quantity";
                $history->quantity = $request->quantity;
                $history->previous_balance = $sales->current_balance;
                $history->current_balance = $total;
                $history->user = $id;
                $sales->previous_balance = $sales->current_balance;
                $sales->current_balance = $total;
                $sales->save();
                $history->save();
                $e = [
                    'type' => 200,
                    'body' => 'Successful',
                    'quantity' => $total,
                    'message' => 'Successfully Added Quantity For '. $request->product
                ];
            }
            else{
                $e = [
                    'type' => 200,
                    'body' => 'Successful',
                    'price' => $sales->current_balance,
                    'message' => 'Price Was Not Changed'
                ];
            }
            $http_response = $e;
        }
        return json_encode($http_response);
    }
    public function sales_sold(Request $request){
        $sales = SalesInventory::where('product_name',$request->product)->first();
        $id = Auth::user()->name;
        if(isset($sales)){
            if($request->quantity != 0){
                if($sales->current_balance >= $request->quantity){
                    if ($request->payment == 1) {
                        $account = new AccountsReceivable();
                        $totot = $request->quantity * $sales->price;
                        $account->account = $request->account;
                        $account->delivery_receipt_number = $request->receipt;
                        $account->product = $request->product;
                        $account->quantity = $request->quantity;
                        $account->price = $sales->price;
                        $account->total_charge = $totot;
                        $account->created_at = Carbon::now('Asia/Manila');
                        $account->updated_at = Carbon::now('Asia/Manila');
                        $account->save();

                        $log = new Log();
                        $log->category = 'Accounts Receivable';
                        $log->action = $request->account.' - Charged &#8369;'.$totot.' on Product:'.$request->product;
                        $log->user = Auth::user()->name;
                        $log->created_at = Carbon::now('Asia/Manila');
                        $log->updated_at = Carbon::now('Asia/Manila');
                        $log->save();
                        $e = [
                            'type' => 200,
                            'body' => 'Successful',
                            'quantity' => $sales->current_balance - $request->quantity,
                            'message' => 'Successfully Sold For '. $request->product.' to Account:'.$request->account
                        ];
                    }else{
                        $e = [
                            'type' => 200,
                            'body' => 'Successful',
                            'quantity' => $sales->current_balance - $request->quantity,
                            'message' => 'Successfully Sold For '. $request->product
                        ];
                    }
                    $log = new Log();
                    $log->category = 'Sales Transaction';
                    $log->action = 'Sold Item: '.$request->product.' - QTY:'.$request->quantity;
                    $log->user = Auth::user()->name;
                    $log->created_at = Carbon::now('Asia/Manila');
                    $log->updated_at = Carbon::now('Asia/Manila');
                    $log->save();

                    $total = $sales->current_balance - $request->quantity;
                    $history = new SalesTransactionHistory();
                    $history->product_name = $request->product;
                    $history->transaction = "Sold Item";
                    $history->quantity = $request->quantity;
                    $history->previous_balance = $sales->current_balance;
                    $history->current_balance = $total;
                    $history->user = $id;
                    $sales->previous_balance = $sales->current_balance;
                    $sales->current_balance = $total;
                    $sales->save();
                    $history->save();

                }else {
                    $e = [
                        'type' => 200,
                        'body' => 'Successful',
                        'price' => $sales->current_balance,
                        'message' => 'Insufficient quantity for ' . $request->product . '; Balance: ' . $sales->current_balance
                    ];
                }
            }
            else{
                $e = [
                    'type' => 200,
                    'body' => 'Successful',
                    'price' => $sales->current_balance,
                    'message' => 'Item was not sold because quantity sold for "'.$request->product.'" was 0'
                ];
            }
            $http_response = $e;
            return json_encode($http_response);
        }
    }
    public function add_customer(Request $request){
        $existing = Customer::where('name',$request->customer)->first();

        if(isset($existing)){
            $e = ['message' => 'Customer Already Existed'];
        }
        else{
            $customer = new Customer();
            $customer->name = $request->customer;
            $customer->save();
            $e = ['message' => 'Successfully Added Customer'];
        }
        return json_encode($e);
    }

}
//}else{
//    $e = [
//        'type' => 200,
//        'body' => 'Successful',
//        'price' => $sales->current_balance,
//        'message' => 'Insufficient quantity for '. $request->product.'; Balance: '.$sales->current_balance
//    ];