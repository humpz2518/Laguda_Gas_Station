<?php

namespace App\Http\Controllers;

use App\AccountsReceivable;
use App\GasolineInventory;
use App\Log;
use App\SalesInventory;
use App\SalesTransactionHistory;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Yajra\Datatables\Datatables;
use App\GasolineTransaction;

class DataTableController extends Controller
{
    public function View_Gasoline_Inventory_Datatable(){
        $data = GasolineInventory::all();

        return Datatables::of($data)
            ->rawColumns(['price','balance','actions'])
            ->addColumn('product', function($model){
                return $model->gasoline_type;
            })
            ->addColumn('price', function($model){
                $price = '<span>&#8369;</span>'.$model->price_per_litre;
                return $price;
            })
            ->addColumn('balance', function($model){
                $balance = $model->current_balance.'<sup> Litres</sup>';
                return $balance;
            })
            ->addColumn('actions', function($model){
                $actions = '<button type="button" data-type="' . $model->gasoline_type . '" data-price="' . $model->price_per_litre . '" class="btn btn-primary update_price">Update Price</button>
                    <button type="button"  data-type="' . $model->gasoline_type . '" data-balance="' . $model->current_balance . '" class="btn btn-success add_litres">Add Gasoline</button>';
                return $actions;
            })
            ->make(true);
    }

    public function View_Sales_Inventory_Datatable(){
        $data = SalesInventory::all();

        return Datatables::of($data)
            ->rawColumns(['price','balance','actions'])
            ->addColumn('barcode', function($model){
                return $model->id;
            })
            ->addColumn('product', function($model){
                return $model->product_name;
            })
            ->addColumn('price', function($model){
                $price = '<span>&#8369;</span>'.$model->price;
                return $price;
            })
            ->addColumn('balance', function($model){
                $balance = $model->current_balance;
                return $balance;
            })
            ->addColumn('actions', function($model){
                $actions = '<button type="button" data-product="' . $model->product_name . '" data-price="' . $model->price . '" class="btn btn-primary sales_update_price">Update Price</button>
                    <button type="button"  data-product="' . $model->product_name . '" data-balance="' . $model->current_balance . '" class="btn btn-success sales_add_quantity">Add Quantity</button>';
                return $actions;
            })
            ->make(true);
    }
    public function View_Gasoline_Transactions_Datatable(){
        $data = GasolineTransaction::all();

        return Datatables::of($data)
            ->addColumn('id', function($model){
                return $model->id;
            })
            ->addColumn('date', function($model){
                $date = $model->created_at->format('F j, Y  g:i A');
                return $date;
            })
            ->addColumn('product', function($model){
                return $model->gasoline_type;
            })
            ->addColumn('transaction', function($model){
                return $model->transaction;
            })
            ->addColumn('litres', function($model){
                return $model->quantity_in_litre;
            })
            ->addColumn('previous_litres', function($model){
                return $model->previous_balance;
            })
            ->addColumn('new_balance', function($model){
                return $model->new_balance;
            })
            ->make(true);
    }

    public function View_Logs_Datatable(){
        $data = Log::all();

        return Datatables::of($data)
            ->addColumn('id', function($model){
                return $model->id;
            })
            ->addColumn('category', function($model){
                return $model->category;
            })
            ->addColumn('action', function($model){
                return $model->action;
            })
            ->addColumn('date', function($model){
                $date = $model->created_at->format('m/d/Y');
                return $date;
            })
            ->addColumn('time', function($model){
                $time = $model->created_at->format('g:i A');
                return $time;
            })
            ->addColumn('user', function($model){
                return $model->user;
            })
            ->make(true);
    }

    public function Accounts_Receivable_Datatable(Request $request){
        if(isset($request->account)){
            if($request->account != 'All'){
                $data = AccountsReceivable::where('account',$request->account);
            }

            else{
                $data = AccountsReceivable::all();
            }
        }

        return Datatables::of($data)
            ->addColumn('date', function ($model) {
                $date = $model->created_at->format('m/d/Y');
                return $date;
            })
            ->addColumn('receipt', function ($model) {
                return $model->delivery_receipt_number;
            })
            ->addColumn('product', function ($model) {
                return $model->product;
            })
            ->addColumn('quantity', function ($model) {
                return $model->quantity;
            })
            ->addColumn('price', function ($model) {
                return $model->price;
            })
            ->addColumn('total_charge', function ($model) {
                return $model->total_charge;
            })
            ->addColumn('account', function ($model) {
                return $model->account;
            })
            ->make(true);
    }
    public function View_Sales_Transaction_Datatable(){
        $data = SalesTransactionHistory::all();

        return Datatables::of($data)
            ->addColumn('id', function($model){
                return $model->id;
            })
            ->addColumn('datetime', function($model){
                return $model->created_at;
            })
            ->addColumn('product', function($model){
                return $model->product_name;
            })
            ->addColumn('transaction', function($model){
                return $model->transaction;
            })
            ->addColumn('quantity', function($model){
                return $model->quantity;
            })
            ->addColumn('prev_quantity', function($model){
                return $model->previous_balance;
            })
            ->addColumn('new_quantity', function($model){
                return $model->current_balance;
            })
            ->addColumn('user', function($model){
                return $model->user;
            })

            ->make(true);
    }

    public function View_Sales_Transaction_Buy_Datatable(){
        $data = SalesInventory::all();

        return Datatables::of($data)
            ->rawColumns(['price','quantity'])
            ->addColumn('id', function($model){
                return $model->id;
            })
            ->addColumn('item', function($model){
                return $model->product_name;
            })
            ->addColumn('price', function($model){
                $price = '<span>&#8369;</span>'.$model->price;
                return $price;
            })
            ->addColumn('quantity', function($model){
//                $actions = '<button type="button" data-product="' . $model->product_name . '" data-price="' . $model->price . '" class="btn btn-primary sales_update_price">Update Price</button>';
                $actions = '<input style="width: 50px;" type="number" min="0" size="4" value="0" id="qty'.$model->id.'"/><button type="button" data-qty="qty'.$model->id.'" data-product="' . $model->product_name . '" data-price="' . $model->price . '" data-balance="' . $model->current_balance . '" class="sales_add_to_list">Add</button>';
                return $actions;
            })

            ->make(true);
    }

    public function View_Sales_Report_Datatable(Request $request){
        $inventory = SalesInventory::all();
        $request->session()->put('day0', Carbon::parse($request->dd0));
        $request->session()->put('day1', Carbon::parse($request->dd1));
        $request->session()->put('day2', Carbon::parse($request->dd2));
        $request->session()->put('day3', Carbon::parse($request->dd3));
        $request->session()->put('day4', Carbon::parse($request->dd4));
        $request->session()->put('day5', Carbon::parse($request->dd5));
        $request->session()->put('day6', Carbon::parse($request->dd6));
        $request->session()->put('day7', Carbon::parse($request->dd7));
        $request->session()->put('day8', Carbon::parse($request->dd8));
        $request->session()->put('tot1',0);
        return Datatables::of($inventory)
            ->addColumn('product', function($model){
                return $model->product_name;
            })
            ->addColumn('beginning_balance', function($model){

                $d = SalesTransactionHistory::where('product_name',$model->product_name)
                    ->where(function ($query) {
                        $value = session('day1');
                        $query->where('created_at','<=',$value)
                            ->orWhere('updated_at','<=',$value);
                    })
                    ->where(function ($query) {
                        $query->where('transaction','Add New Item')
                            ->orWhere('transaction','Add Quantity')
                            ->orWhere('transaction','Sold Item');
                    })
                    ->orderBy('updated_at', 'desc')->first();
                if(isset($d)){

                    session()->put('currbal',$d->current_balance);
                    return $d->current_balance;
                }else{
                    session()->put('prevbal',0);
                    session()->put('currbal',0);
                    return 0;
                }
            })
            ->addColumn('additional_quantity', function($model){
                $value = session('day1');
                $value2 = session('day8');
                $e = SalesTransactionHistory::where('created_at','>=',$value)
                    ->where('created_at','<',$value2)
                    ->where('product_name',$model->product_name)
                    ->where(function ($query) {
                        $query->where('transaction','Add New Item')
                            ->orWhere('transaction','Add Quantity');
                    })
                    ->sum('quantity');
                if(isset($e)){
                    $total = $e;
                }else{
                    $total = 0;
                }
                session()->put('addquant',$total);
                return $total;
            })
            ->addColumn('total_quantity', function($model){
                $value1 = session('currbal');
                $value2 = session('addquant');
                $total = $value1 + $value2;
                session()->put('totquant',$total);
                return $total;
            })
            ->addColumn('d1', function($model){
                $value = session('day1');
                $value2 = session('day2');
                $d = SalesTransactionHistory::where('created_at','>=',$value)
                    ->where('created_at','<',$value2)
                    ->where('product_name',$model->product_name)
                    ->where('transaction','Sold Item')
                    ->sum('quantity');
                if(isset($d)){
                    session()->put('day1total',$d);
                    return $d;
                }else{
                    session()->put('day1total',0);
                    return 0;
                }

            })
            ->addColumn('d2', function($model){
                $value = session('day2');
                $value2 = session('day3');
                $d = SalesTransactionHistory::where('created_at','>=',$value)
                    ->where('created_at','<',$value2)
                    ->where('product_name',$model->product_name)
                    ->where('transaction','Sold Item')
                    ->sum('quantity');
                if(isset($d)){
                    session()->put('day2total',$d);
                    return $d;
                }else{
                    session()->put('day2total',0);
                    return 0;
                }
            })
            ->addColumn('d3', function($model){
                $value = session('day3');
                $value2 = session('day4');
                $d = SalesTransactionHistory::where('created_at','>=',$value)
                    ->where('created_at','<',$value2)
                    ->where('product_name',$model->product_name)
                    ->where('transaction','Sold Item')
                    ->sum('quantity');
                if(isset($d)){
                    session()->put('day3total',$d);
                    return $d;
                }else{
                    session()->put('day3total',0);
                    return 0;
                }
            })
            ->addColumn('d4', function($model){
                $value = session('day4');
                $value2 = session('day5');
                $d = SalesTransactionHistory::where('created_at','>=',$value)
                    ->where('created_at','<',$value2)
                    ->where('product_name',$model->product_name)
                    ->where('transaction','Sold Item')
                    ->sum('quantity');
                if(isset($d)){
                    session()->put('day4total',$d);
                    return $d;
                }else{
                    session()->put('day4total',0);
                    return 0;
                }
            })
            ->addColumn('d5', function($model){
                $value = session('day5');
                $value2 = session('day6');
                $d = SalesTransactionHistory::where('created_at','>=',$value)
                    ->where('created_at','<',$value2)
                    ->where('product_name',$model->product_name)
                    ->where('transaction','Sold Item')
                    ->sum('quantity');
                if(isset($d)){
                    session()->put('day5total',$d);
                    return $d;
                }else{
                    session()->put('day5total',0);
                    return 0;
                }
            })
            ->addColumn('d6', function($model){
                $value = session('day6');
                $value2 = session('day7');
                $d = SalesTransactionHistory::where('created_at','>=',$value)
                    ->where('created_at','<',$value2)
                    ->where('product_name',$model->product_name)
                    ->where('transaction','Sold Item')
                    ->sum('quantity');
                if(isset($d)){
                    session()->put('day6total',$d);
                    return $d;
                }else{
                    session()->put('day6total',0);
                    return 0;
                }
            })
            ->addColumn('d7', function($model){
                $value = session('day7');
                $value2 = session('day8');
                $d = SalesTransactionHistory::where('created_at','>=',$value)
                    ->where('created_at','<',$value2)
                    ->where('product_name',$model->product_name)
                    ->where('transaction','Sold Item')
                    ->sum('quantity');
                if(isset($d)){
                    session()->put('day7total',$d);
                    return $d;
                }else{
                    session()->put('day7total',0);
                    return 0;
                }
            })
            ->addColumn('total_sales', function($model){
                $total=session('day1total')+session('day2total')+session('day3total')+session('day4total')+session('day5total')+session('day6total')+session('day7total');
                session()->put('totaling',$total);
                return $total;
            })
            ->addColumn('ending_balance', function($model){
                $val1 = session('totquant');
                $val2 = session('totaling');
                $total = $val1 - $val2;
                return $total;
            })
            ->make(true);
    }

    public function View_Gasoline_Report_Datatable(Request $request){
        $inventory = GasolineInventory::all();
        $request->session()->put('day0', Carbon::parse($request->dd0));
        $request->session()->put('day1', Carbon::parse($request->dd1));
        $request->session()->put('day2', Carbon::parse($request->dd2));
        $request->session()->put('day3', Carbon::parse($request->dd3));
        $request->session()->put('day4', Carbon::parse($request->dd4));
        $request->session()->put('day5', Carbon::parse($request->dd5));
        $request->session()->put('day6', Carbon::parse($request->dd6));
        $request->session()->put('day7', Carbon::parse($request->dd7));
        $request->session()->put('day8', Carbon::parse($request->dd8));
        $request->session()->put('tot1',0);
        return Datatables::of($inventory)
            ->addColumn('product', function($model){
                return $model->gasoline_type;
            })
            ->addColumn('beginning_balance', function($model){
                $value = session('day1');
                $d = GasolineTransaction::where('gasoline_type',$model->gasoline_type)
                    ->where('created_at','<=',$value)
                    ->where('transaction','Add')
                    ->orderBy('created_at', 'desc')->first();
                if(isset($d)){

                    session()->put('currbal',$d->new_balance);
                    return $d->new_balance;
                }else{
                    session()->put('prevbal',0);
                    session()->put('currbal',0);
                    return 0;
                }
            })
            ->addColumn('additional_litres', function($model){
                $value = session('day1');
                $value2 = session('day8');
                $e = GasolineTransaction::where('created_at','>=',$value)
                    ->where('created_at','<',$value2)
                    ->where('gasoline_type',$model->gasoline_type)
                    ->where('transaction','Add')
                    ->sum('quantity_in_litre');
                if(isset($e)){
                    $total = $e;
                }else{
                    $total = 0;
                }
                session()->put('addquant',$total);
                return $total;
            })
            ->addColumn('total_litres', function($model){
                $value1 = session('currbal');
                $value2 = session('addquant');
                $total = $value1 + $value2;
                session()->put('totquant',$total);
                return $total;
            })
            ->addColumn('d1', function($model){
                $value = session('day1');
                $value2 = session('day2');
                $d = GasolineTransaction::where('created_at','>=',$value)
                    ->where('created_at','<',$value2)
                    ->where('gasoline_type',$model->gasoline_type)
                    ->where('transaction','Sold')
                    ->sum('quantity_in_litre');
                if(isset($d)){
                    session()->put('day1total',$d);
                    return $d;
                }else{
                    session()->put('day1total',0);
                    return 0;
                }

            })
            ->addColumn('d2', function($model){
                $value = session('day2');
                $value2 = session('day3');
                $d = GasolineTransaction::where('created_at','>=',$value)
                    ->where('created_at','<',$value2)
                    ->where('gasoline_type',$model->gasoline_type)
                    ->where('transaction','Sold')
                    ->sum('quantity_in_litre');
                if(isset($d)){
                    session()->put('day2total',$d);
                    return $d;
                }else{
                    session()->put('day2total',0);
                    return 0;
                }
            })
            ->addColumn('d3', function($model){
                $value = session('day3');
                $value2 = session('day4');
                $d = GasolineTransaction::where('created_at','>=',$value)
                    ->where('created_at','<',$value2)
                    ->where('gasoline_type',$model->gasoline_type)
                    ->where('transaction','Sold')
                    ->sum('quantity_in_litre');
                if(isset($d)){
                    session()->put('day3total',$d);
                    return $d;
                }else{
                    session()->put('day3total',0);
                    return 0;
                }
            })
            ->addColumn('d4', function($model){
                $value = session('day4');
                $value2 = session('day5');
                $d = GasolineTransaction::where('created_at','>=',$value)
                    ->where('created_at','<',$value2)
                    ->where('gasoline_type',$model->gasoline_type)
                    ->where('transaction','Sold')
                    ->sum('quantity_in_litre');
                if(isset($d)){
                    session()->put('day4total',$d);
                    return $d;
                }else{
                    session()->put('day4total',0);
                    return 0;
                }
            })
            ->addColumn('d5', function($model){
                $value = session('day5');
                $value2 = session('day6');
                $d = GasolineTransaction::where('created_at','>=',$value)
                    ->where('created_at','<',$value2)
                    ->where('gasoline_type',$model->gasoline_type)
                    ->where('transaction','Sold')
                    ->sum('quantity_in_litre');
                if(isset($d)){
                    session()->put('day5total',$d);
                    return $d;
                }else{
                    session()->put('day5total',0);
                    return 0;
                }
            })
            ->addColumn('d6', function($model){
                $value = session('day6');
                $value2 = session('day7');
                $d = GasolineTransaction::where('created_at','>=',$value)
                    ->where('created_at','<',$value2)
                    ->where('gasoline_type',$model->gasoline_type)
                    ->where('transaction','Sold')
                    ->sum('quantity_in_litre');
                if(isset($d)){
                    session()->put('day6total',$d);
                    return $d;
                }else{
                    session()->put('day6total',0);
                    return 0;
                }
            })
            ->addColumn('d7', function($model){
                $value = session('day7');
                $value2 = session('day8');
                $d = GasolineTransaction::where('created_at','>=',$value)
                    ->where('created_at','<',$value2)
                    ->where('gasoline_type',$model->gasoline_type)
                    ->where('transaction','Sold')
                    ->sum('quantity_in_litre');
                if(isset($d)){
                    session()->put('day7total',$d);
                    return $d;
                }else{
                    session()->put('day7total',0);
                    return 0;
                }
            })
            ->addColumn('total_sales', function($model){
                $total=session('day1total')+session('day2total')+session('day3total')+session('day4total')+session('day5total')+session('day6total')+session('day7total');
                session()->put('totaling',$total);
                return $total;
            })
            ->addColumn('ending_balance', function($model){
                $val1 = session('totquant');
                $val2 = session('totaling');
                $total = $val1 - $val2;
                return $total;
            })


            ->make(true);
    }


}
