<?php

namespace App\Http\Controllers;

use App\AccountsReceivable;
use App\GasolineInventory;
use App\Log;
use Illuminate\Http\Request;
use App\Customer;
use Illuminate\Support\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $logs = Log::where('created_at','>=',Carbon::today('Asia/Manila'))
                    ->where('created_at','<',Carbon::tomorrow('Asia/Manila'))
                    ->orderBy('created_at','desc')
                    ->take(5)
                    ->get();

        $gas_reports = GasolineInventory::all();
        $receivables = AccountsReceivable::where('created_at','>=',Carbon::today('Asia/Manila'))
            ->where('created_at','<',Carbon::tomorrow('Asia/Manila'))
            ->orderBy('created_at','desc')
            ->take(5)
            ->get();

        return view('home',['logs' => $logs, 'gas_reports' => $gas_reports,'receivables' => $receivables]);
    }

    public function accounts_receivable()
    {
        $customers = Customer::all();
        return view('accounts-receivable',['customers' => $customers]);
    }
    public function gasoline_reports()
    {
        return view('gasoline-reports');
    }
    public function sales_reports()
    {
        return view('sales-reports');
    }
    public function gasoline_inventory()
    {
        return view('gasoline-inventory');
    }
    public function sales_inventory()
    {
        return view('sales-inventory');
    }
    
    public function gas_transaction()
    {
        $gasolines = GasolineInventory::all();
        $customers = Customer::all();
        return view('gas-transaction',['gasolines' => $gasolines, 'customers' => $customers]);
    }

    public function sales_transaction()
    {
        $customers = Customer::all();
        return view('sales-transaction',['customers' => $customers]);
    }

    public function logs()
    {
        return view('logs');
    }
}
