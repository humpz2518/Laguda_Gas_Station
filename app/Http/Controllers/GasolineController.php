<?php

namespace App\Http\Controllers;

use App\AccountsReceivable;
use App\Customer;
use App\GasolineInventory;
use App\GasolineTransaction;
use App\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class GasolineController extends Controller
{
    public function save_gasoline_price(Request $request){
        $gasoline = GasolineInventory::where('gasoline_type',$request->type)->first();
        $old_price = $gasoline->price_per_litre;
        if(isset($gasoline)){
            if($gasoline->price_per_litre != $request->price){
                $gasoline->price_per_litre = $request->price;
                $gasoline->save();
                $e = [
                    'type' => 200,
                    'body' => 'Successful',
                    'price' => $gasoline->price_per_litre,
                    'message' => 'Successfully Updated Price For '. $request->type
                ];
                $log = new Log();
                $log->category = 'Gasoline Inventory';
                $log->action = 'Updated Price Of '.$request->type.' From <span>&#8369;</span>'.$old_price.' To <span>&#8369;</span>'.$request->price;
                $log->user = Auth::user()->name;
                $log->created_at = Carbon::now('Asia/Manila');
                $log->updated_at = Carbon::now('Asia/Manila');
                $log->save();
            }
            else{
                $e = [
                    'type' => 200,
                    'body' => 'Successful',
                    'price' => $gasoline->price_per_litre,
                    'message' => 'Price Was Not Changed'
                ];
            }
            $http_response = $e;
        }
        return json_encode($http_response);
    }

    public function add_gasoline_litres(Request $request){
        $gasoline = GasolineInventory::where('gasoline_type',$request->type)->first();
        if(isset($gasoline)){
            $gasoline->previous_balance = $request->balance;
            $gasoline->current_balance = $request->balance+$request->litres;
            if($gasoline->save()){
                $e = [
                    'type' => 200,
                    'body' => 'Successful',
                    'balance' => $gasoline->current_balance,
                    'message' => 'Successfully Added '.$request->litres.' Litres For '. $request->type
                ];
                $transaction = new GasolineTransaction();
                $transaction->transaction = 'Add';
                $transaction->gasoline_type = $request->type;
                $transaction->quantity_in_litre = $request->litres;
                $transaction->previous_balance = $request->balance;
                $transaction->new_balance = $request->balance+$request->litres;
                $transaction->user = Auth::user()->name;
                $transaction->created_at = Carbon::now('Asia/Manila');
                $transaction->updated_at = Carbon::now('Asia/Manila');
                $transaction->save();

                $log = new Log();
                $log->category = 'Gasoline Inventory';
                $log->action = 'Added '.$request->type.' - '.$request->litres.'L';
                $log->user = Auth::user()->name;
                $log->created_at = Carbon::now('Asia/Manila');
                $log->updated_at = Carbon::now('Asia/Manila');
                $log->save();
            }
            else{
                $e = [
                    'type' => 400,
                    'body' => 'Failed',
                    'message' => 'Failed To Add'
                ];
            }

        }
        else{
            $e = [
                'type' => 400,
                'body' => 'Failed',
                'message' => 'Nothing to Add'
            ];
        }
        $http_response = $e;
        return json_encode($http_response);
    }

    public function get_litres(Request $request){
        $gasoline = GasolineInventory::where('gasoline_type',$request->type)->first();

        if(isset($gasoline)){
            $litres = $request->amount/$gasoline->price_per_litre;
            $e = ['litres' => round($litres, 2)];
        }
        return json_encode($e);
    }

    public function save_transaction(Request $request){
        $gasoline = GasolineInventory::where('gasoline_type',$request->type)->first();
        if(isset($gasoline)){
            if(!isset($request->amount)){
                $e = ['message' => 'Please input amount.'];
            }
            else{
                if($request->litres > $gasoline->current_balance){
                    $e = ['message' => 'The gasoline balance is lower than the requested litres.'];
                }
                else{
                    if(isset($request->payment)) {
                        if ($request->payment == 'Cash') {
                            $transaction = new GasolineTransaction();
                            $transaction->transaction = 'Sold';
                            $transaction->gasoline_type = $request->type;
                            $transaction->quantity_in_litre = $request->litres;
                            $transaction->previous_balance = $gasoline->current_balance;
                            $transaction->new_balance = $gasoline->current_balance - $request->litres;
                            $transaction->user = Auth::user()->name;
                            $transaction->created_at = Carbon::now('Asia/Manila');
                            $transaction->updated_at = Carbon::now('Asia/Manila');
                            $transaction->save();

                            $log = new Log();
                            $log->category = 'Gasoline Transaction';
                            $log->action = 'Sold '.$request->type.' - '.$request->litres.'L';
                            $log->user = Auth::user()->name;
                            $log->created_at = Carbon::now('Asia/Manila');
                            $log->updated_at = Carbon::now('Asia/Manila');
                            $log->save();

                            $gasoline->previous_balance = $gasoline->current_balance;
                            $gasoline->current_balance = $gasoline->current_balance - $request->litres;
                            $gasoline->save();

                            $e = ['message' => 'Successfully Made Transaction'];

                        }
                        elseif($request->payment == 'Account'){

                            $transaction = new GasolineTransaction();
                            $transaction->transaction = 'Sold';
                            $transaction->gasoline_type = $request->type;
                            $transaction->quantity_in_litre = $request->litres;
                            $transaction->previous_balance = $gasoline->current_balance;
                            $transaction->new_balance = $gasoline->current_balance - $request->litres;
                            $transaction->user = Auth::user()->name;
                            $transaction->created_at = Carbon::now('Asia/Manila');
                            $transaction->updated_at = Carbon::now('Asia/Manila');
                            $transaction->save();

                            $log = new Log();
                            $log->category = 'Gasoline Transaction';
                            $log->action = 'Sold '.$request->type.' - '.$request->litres.'L';
                            $log->user = Auth::user()->name;
                            $log->created_at = Carbon::now('Asia/Manila');
                            $log->updated_at = Carbon::now('Asia/Manila');
                            $log->save();

                            $log = new Log();
                            $log->category = 'Accounts Receivable';
                            $log->action = $request->account.' - Charged &#8369;'.$request->amount;
                            $log->user = Auth::user()->name;
                            $log->created_at = Carbon::now('Asia/Manila');
                            $log->updated_at = Carbon::now('Asia/Manila');
                            $log->save();

                            if(isset($request->account)){
                                $account = new AccountsReceivable();
                                $account->account = $request->account;
                                $account->delivery_receipt_number = $request->receipt;
                                $account->product = $request->type;
                                $account->quantity = $request->litres;
                                $account->price = $gasoline->price_per_litre;
                                $account->total_charge = $request->amount;
                                $account->created_at = Carbon::now('Asia/Manila');
                                $account->updated_at = Carbon::now('Asia/Manila');
                                $account->save();
                            }

                            else{
                                $e = ['message' => 'Please Select Account'];
                            }

                            $gasoline->previous_balance = $gasoline->current_balance;
                            $gasoline->current_balance = $gasoline->current_balance - $request->litres;
                            $gasoline->save();

                            $e = ['message' => 'Successfully Made Transaction'];

                        }
                    }
                    else{
                        $e = ['message' => 'Please Input Payment Method'];
                    }
                }
            }
        }
        else{
            $e = ['message' => 'Please Input Gasoline Type'];
        }

        return json_encode($e);
    }

    public function add_customer(Request $request){
        $existing = Customer::where('name',$request->customer)->first();

        if(isset($existing)){
            $e = ['message' => 'Customer Already Existed'];
        }
        else{
            $customer = new Customer();
            $customer->name = $request->customer;
            $customer->save();
            $e = ['message' => 'Successfully Added Customer'];
        }
        return json_encode($e);
    }
}
