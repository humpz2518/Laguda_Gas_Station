<?php

namespace App\Http\Controllers;

use App\AccountsReceivable;
use App\GasolineInventory;
use App\GasolineTransaction;
use App\SalesInventory;
use App\SalesTransaction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Symfony\Component\HttpFoundation\Cookie;

class CSVController extends Controller
{
    public function gasoline_inventory() {
        $now = Carbon::now('Asia/Manila')->format('m/d/Y g:i A');
        $results = GasolineInventory::all();

        $dataset = [];

        foreach($results as $result){
            $dataset[] = [
                $result->gasoline_type,
                $result->price_per_litre,
                $result->current_balance,
            ];
        }

        $table_headers = [
            'Type',
            'Price Per Litre(in Peso)',
            'Current Balance (Litres)'
        ];

        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private", false);
        header("Content-Type: application/octet-stream");
        header("Content-Disposition: attachment; filename=\"GasolineInventory $now.csv\";" );
        header("Content-Transfer-Encoding: binary");

        $FH = fopen('php://output', 'w');
        fputcsv($FH, $table_headers);

        foreach ($dataset as $row) {
            fputcsv($FH, $row);
        }

        exit();

    }

    public function accounts_receivable(Request $request) {
        $now = Carbon::now('Asia/Manila')->format('m/d/Y g:i A');
        if(isset($_COOKIE['account'])){
            $account = $_COOKIE['account'];
            $account_name = $account;
        }

        else{
            $account_name = 'All';
        }

        if($account_name != 'All'){
            $results = AccountsReceivable::where('account',$account_name)->get();
        }

        else{
            $results = AccountsReceivable::all();
        }

        $dataset = [];

        foreach($results as $result){
            $date = $result->created_at->format('m/d/Y');
            $dataset[] = [
                $date,
                $result->delivery_receipt_number,
                $result->product,
                $result->quantity,
                $result->price,
                $result->total_charge,
                $result->account,
            ];
        }

        $table_headers = [
            'Date',
            'Delivery Receipt No.',
            'Product',
            'No. of ltrs / Qty',
            'Price',
            'Total Charge',
            'Account Name'
        ];

        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private", false);
        header("Content-Type: application/octet-stream");
        header("Content-Disposition: attachment; filename=\"AccountsReceivable $now.csv\";" );
        header("Content-Transfer-Encoding: binary");

        $FH = fopen('php://output', 'w');
        fputcsv($FH, $table_headers);

        foreach ($dataset as $row) {
            fputcsv($FH, $row);
        }

        exit();

    }

    public function sales_inventory() {
        $now = Carbon::now('Asia/Manila')->format('m/d/Y g:i A');
        $results = SalesInventory::all();

        $dataset = [];

        foreach($results as $result){
            $dataset[] = [
                $result->id,
                $result->product_name,
                $result->price,
                $result->current_balance
            ];
        }

        $table_headers = [
            'Barcode',
            'Product',
            'Price (in Peso)',
            'Balance (Quantity)'
        ];

        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private", false);
        header("Content-Type: application/octet-stream");
        header("Content-Disposition: attachment; filename=\"SalesInventory $now.csv\";" );
        header("Content-Transfer-Encoding: binary");

        $FH = fopen('php://output', 'w');
        fputcsv($FH, $table_headers);

        foreach ($dataset as $row) {
            fputcsv($FH, $row);
        }

        exit();

    }

    public function gasoline_reports(Request $request){
        $results = GasolineInventory::all();
        $date = $_COOKIE['date'];
        $header_days = [];
        $dataset = [];

        for($x = 0;$x<=7;$x++){
            array_push($header_days,Carbon::parse($date)->addDay($x));
        }

        foreach($results as $result){
            $value = $header_days[0];
            $valueminus1 = Carbon::parse($value)->subDay(1);
            $d = GasolineTransaction::where('created_at','<',$value)
                ->where('gasoline_type',$result->gasoline_type)
                ->where('transaction','Add')
                ->orderBy('created_at', 'desc')->first();
            if(isset($d)){
                $current_bal =  $d->new_balance;
            }else{
                $current_bal =  0;
            }

            $e = GasolineTransaction::where('created_at','>=',$value)
                ->where('created_at','<',$header_days[6])
                ->where('gasoline_type',$result->gasoline_type)
                ->where('transaction','Add')
                ->sum('quantity_in_litre');

            if(isset($e)){
                $add_litres =  $e;
            }else{
                $add_litres =  0;
            }

            $total = $current_bal + $add_litres;

            $d1 = GasolineTransaction::where('created_at','>=',$header_days[0])
                ->where('created_at','<',$header_days[1])
                ->where('gasoline_type',$result->gasoline_type)
                ->where('transaction','Sold')
                ->sum('quantity_in_litre');

            if(!isset($d1)){
                $d1 =  0;
            }

            $d2 = GasolineTransaction::where('created_at','>=',$header_days[1])
                ->where('created_at','<',$header_days[2])
                ->where('gasoline_type',$result->gasoline_type)
                ->where('transaction','Sold')
                ->sum('quantity_in_litre');

            if(!isset($d1)){
                $d2 =  0;
            }

            $d3 = GasolineTransaction::where('created_at','>=',$header_days[2])
                ->where('created_at','<',$header_days[3])
                ->where('gasoline_type',$result->gasoline_type)
                ->where('transaction','Sold')
                ->sum('quantity_in_litre');

            if(!isset($d1)){
                $d3 =  0;
            }

            $d4 = GasolineTransaction::where('created_at','>=',$header_days[3])
                ->where('created_at','<',$header_days[4])
                ->where('gasoline_type',$result->gasoline_type)
                ->where('transaction','Sold')
                ->sum('quantity_in_litre');

            if(!isset($d1)){
                $d4 =  0;
            }

            $d5 = GasolineTransaction::where('created_at','>=',$header_days[4])
                ->where('created_at','<',$header_days[5])
                ->where('gasoline_type',$result->gasoline_type)
                ->where('transaction','Sold')
                ->sum('quantity_in_litre');

            if(!isset($d1)){
                $d5 =  0;
            }

            $d6 = GasolineTransaction::where('created_at','>=',$header_days[5])
                ->where('created_at','<',$header_days[6])
                ->where('gasoline_type',$result->gasoline_type)
                ->where('transaction','Sold')
                ->sum('quantity_in_litre');

            if(!isset($d1)){
                $d6 =  0;
            }

            $d7 = GasolineTransaction::where('created_at','>=',$header_days[6])
                ->where('created_at','<',$header_days[7])
                ->where('gasoline_type',$result->gasoline_type)
                ->where('transaction','Sold')
                ->sum('quantity_in_litre');

            if(!isset($d1)){
                $d7 =  0;
            }

            $total_sales = $d1 + $d2 + $d3 + $d4 + $d5 + $d6 + $d7;
            $ending_balance = $total - $total_sales;
            $dataset[] = [
                $result->gasoline_type,
                $current_bal,
                $add_litres,
                $total,
                $d1,
                $d2,
                $d3,
                $d4,
                $d5,
                $d6,
                $d7,
                $total_sales,
                $ending_balance
            ];
        }

        $table_headers = [
            'Product',
            'Beginning Balance',
            'Additional Litres',
            'Total Litres',
            $header_days[0]->format('d'),
            $header_days[1]->format('d'),
            $header_days[2]->format('d'),
            $header_days[3]->format('d'),
            $header_days[4]->format('d'),
            $header_days[5]->format('d'),
            $header_days[6]->format('d'),
            'Total Sales',
            'Ending Balance'
        ];

        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private", false);
        header("Content-Type: application/octet-stream");
        header("Content-Disposition: attachment; filename=\"GasolineReports $date.csv\";" );
        header("Content-Transfer-Encoding: binary");

        $FH = fopen('php://output', 'w');
        fputcsv($FH, $table_headers);

        foreach ($dataset as $row) {
            fputcsv($FH, $row);
        }

        exit();

    }

    public function sales_reports(){
        $results = SalesInventory::all();
        $date = $_COOKIE['date'];
        $header_days = [];

        for($x = 0;$x<=7;$x++){
            array_push($header_days,Carbon::parse($date)->addDay($x));
        }


        foreach($results as $result){
            $value = $header_days[0];
            $valueminus1 = Carbon::parse($value)->subDay(1);

            $d = SalesTransaction::where('created_at','<',$value)
                ->where('product_name',$result->product_name)
                ->where(function ($query) {
                    $query->where('transaction','Add New Item')
                        ->orWhere('transaction','Add Quantity');
                })
                ->orderBy('created_at', 'desc')->first();

            if(isset($d)){
                $current_bal =  $d->current_balance;
            }else{
                $current_bal =  0;
            }

            $e = SalesTransaction::where('created_at','>=',$value)
                ->where('created_at','<',$header_days[6])
                ->where('product_name',$result->product_name)
                ->where('transaction','Add Quantity')
                ->sum('quantity');

            if(isset($e)){
                $add_quantity =  $e;
            }else{
                $add_quantity =  0;
            }

            $total = $current_bal + $add_quantity;

            $d1 = SalesTransaction::where('created_at','>=',$header_days[0])
                ->where('created_at','<',$header_days[1])
                ->where('product_name',$result->product_name)
                ->where('transaction','Sold Item')
                ->sum('quantity');

            if(!isset($d1)){
                $d1 =  0;
            }

            $d2 = SalesTransaction::where('created_at','>=',$header_days[1])
                ->where('created_at','<',$header_days[2])
                ->where('product_name',$result->product_name)
                ->where('transaction','Sold Item')
                ->sum('quantity');

            if(!isset($d2)){
                $d2 =  0;
            }

            $d3 = SalesTransaction::where('created_at','>=',$header_days[2])
                ->where('created_at','<',$header_days[3])
                ->where('product_name',$result->product_name)
                ->where('transaction','Sold Item')
                ->sum('quantity');

            if(!isset($d3)){
                $d3 =  0;
            }

            $d4 = SalesTransaction::where('created_at','>=',$header_days[3])
                ->where('created_at','<',$header_days[4])
                ->where('product_name',$result->product_name)
                ->where('transaction','Sold Item')
                ->sum('quantity');

            if(!isset($d4)){
                $d4 =  0;
            }

            $d5 = SalesTransaction::where('created_at','>=',$header_days[4])
                ->where('created_at','<',$header_days[5])
                ->where('product_name',$result->product_name)
                ->where('transaction','Sold Item')
                ->sum('quantity');

            if(!isset($d5)){
                $d5 =  0;
            }

            $d6 = SalesTransaction::where('created_at','>=',$header_days[5])
                ->where('created_at','<',$header_days[6])
                ->where('product_name',$result->product_name)
                ->where('transaction','Sold Item')
                ->sum('quantity');

            if(!isset($d6)){
                $d6 =  0;
            }

            $d7 = SalesTransaction::where('created_at','>=',$header_days[6])
                ->where('created_at','<',$header_days[7])
                ->where('product_name',$result->product_name)
                ->where('transaction','Sold Item')
                ->sum('quantity');

            if(!isset($d7)){
                $d7 =  0;
            }

            $total_sales = $d1 + $d2 + $d3 + $d4 + $d5 + $d6 + $d7;
            $ending_balance = $total - $total_sales;

            $dataset[] = [
                $result->product_name,
                $current_bal,
                $add_quantity,
                $total,
                $d1,
                $d2,
                $d3,
                $d4,
                $d5,
                $d6,
                $d7,
                $total_sales,
                $ending_balance
            ];
        }

        $table_headers = [
            'Product',
            'Beginning Balance',
            'Additional Quantity',
            'Total Quantity',
            $header_days[0]->format('d'),
            $header_days[1]->format('d'),
            $header_days[2]->format('d'),
            $header_days[3]->format('d'),
            $header_days[4]->format('d'),
            $header_days[5]->format('d'),
            $header_days[6]->format('d'),
            'Total Sales',
            'Ending Balance'
        ];

        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private", false);
        header("Content-Type: application/octet-stream");
        header("Content-Disposition: attachment; filename=\"SalesReports $date.csv\";" );
        header("Content-Transfer-Encoding: binary");

        $FH = fopen('php://output', 'w');
        fputcsv($FH, $table_headers);

        foreach ($dataset as $row) {
            fputcsv($FH, $row);
        }

        exit();
    }
}
