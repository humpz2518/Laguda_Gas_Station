@extends('layouts.dashboard')

@section('content')
    <div class="block-header">
        <h2>Accounts Receivable</h2>
    </div>

    <div class="row clearfix">
        <!-- Task Info -->
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="header">
                    <form method="GET" action="{{route('accounts-receivable-csv')}}" class="pull-right">
                    <button class="btn btn-success"  type="submit" id="accounts_receivable_csv">Download CSV</button>
                    </form>
                    <i class="material-icons pull-right" style="margin-right: 10px;font-size: 30px; cursor:pointer;" id="add_icon">person_add</i>
                    <h5>
                        Name of Customer:
                        <select name="name" id="name">
                            <option>All</option>
                            @foreach($customers as $customer)
                            <option>{{$customer->name}}</option>
                            @endforeach
                        </select>
                    </h5>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-hover dashboard-task-infos" id="accounts_receivable">
                            <thead>
                            <tr>
                                <th>Date</th>
                                <th>Delivery Receipt No.</th>
                                <th>Product</th>
                                <th>No. of ltrs / Qty</th>
                                <th>Price</th>
                                <th>Total Charge</th>
                                <th>Account Name</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="add_customer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Customer</h5>
                </div>
                <div class="modal-body">
                    <form method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="col-sm-12">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" name="customer_name">
                                    <label class="form-label">Customer's Full Name</label>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="save_customer">Save Customer</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready( function () {
            data_table.init();
        } );

        var data_table = {
            data : {},
            init : function() {
                var accounts_receivable_table = $('#accounts_receivable');
                var account = $("#name").val();
                accounts_receivable_table.DataTable({
                    serverSide: true,
                    autoWidth: false,
                    retrieve: true,
                    ajax: {
                        "url": '/accounts_receivable/datatable',
                        "data": function ( d ) {
                            d.account = $('#name').val();
                        }
                    },
                    columns: [
                        {data: 'date', name: 'date',orderable: true},
                        {data: 'receipt', name: 'receipt',orderable: false},
                        {data: 'product', name: 'product',orderable: false},
                        {data: 'quantity', name: 'quantity',orderable: false},
                        {data: 'price', name: 'price',orderable: false},
                        {data: 'total_charge', name: 'total_charge',orderable: false},
                        {data: 'account', name: 'account',orderable: false},
                    ],

                    "dom": 'lCfrtip',
                    "order": [],
                    "colVis": {
                        "buttonText": "Columns",
                        "overlayFade": 0,
                        "align": "right"
                    },
                    "language": {
                        "lengthMenu": '_MENU_ entries per page',
                        "search": 'Search:<i class="fa fa-search" style=""></i>',
                        "paginate": {
                            "previous": '<i class="fa fa-angle-left"></i>',
                            "next": '<i class="fa fa-angle-right"></i>'
                        }
                    },
                    fnDrawCallback : function(){
                        $("#name").one("change", function() {
                            $.cookie("account", $(this).val(), { expires : 10, path: '/' });
                            alert($.cookie("account"));
                            $('#accounts_receivable').dataTable().fnDraw();
                        });
                    }
                });
            }
        };

        $('#add_icon').one("click", function() {
            $('#add_customer').modal({
                show: 'true'
            });
        });

        $('#save_customer').click(function(){
            $('#add_customer').modal('hide');
            var customer = $('#add_customer input[name=customer_name]').val();
            $.ajax({
                url: '/accounts_receivable/add_customer',
                method: 'POST',
                data: {
                    _token: $('#add_customer input[name=_token]').val(),
                    customer: customer
                },
                dataType: 'json',
                success: function(data){
                    alert(data.message);
                    location. reload(true);
                },
                error: function (data) {
                    console.log(data);
                }
            });
        });
    </script>
@endsection
