@extends('layouts.dashboard')

@section('content')
    <div class="block-header">
        <h2>Gasoline Reports</h2>
    </div>

    <div class="row clearfix">
        <!-- Task Info -->
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <form method="GET" action="{{route('gasoline-reports-csv')}}" class="pull-right">
                        <button class="btn btn-success" type="submit">Download CSV</button>
                    </form>
                    <h4>LAGUDA GAS STATION</h4>
                    <h5>Cor. Broce / S. Carmona Street, San Carlos City , Negros Occidental 6127</h5>
                    <h5 id="periodcover"></h5>
                    <div class="row clearfix">
                        <form method="GET" action="#">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="datepicker form-control" id="start_date" placeholder="Please choose a date...">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <button class="btn btn-primary btn-large filtering" type="button" onclick="getdate()">Filter Dates</button>
                                <button class="btn btn-warning btn-large resetting" type="button" onclick="location.reload(true)">Reset</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-hover dashboard-task-infos" id="gasoline_reports_table">
                            <thead>
                            <tr>
                                <th>Product</th>
                                <th>Beginning Balance</th>
                                <th>Additional Litres</th>
                                <th>Total Litres</th>
                                <th id="d1"></th>
                                <th id="d2"></th>
                                <th id="d3"></th>
                                <th id="d4"></th>
                                <th id="d5"></th>
                                <th id="d6"></th>
                                <th id="d7"></th>
                                <th>Total Sales</th>
                                <th>Ending Balance</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        var D0, D1, D2, D3, D4, D5, D6, D7, D8;
        const monthNames = ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"
        ];
        $(document).ready(function(){
            $(".body").hide();
            $(".resetting").hide();
            $('#start_date').bootstrapMaterialDatePicker({
                weekStart : 0,
                time: false
            });
            autosize($('textarea.auto-growth'));
        });
        function getdate(){
            $(".filtering").hide();
            $(".body").show();
            $(".resetting").show();
            var dit = $('#start_date').val();
            $.cookie("date", $('#start_date').val(), { expires : 10, path: '/' });
            alert(dit);

            //Codes for the 7 days
            var myDate = new Date(dit);
            var myDate0 = new Date(dit);

            myDate0.setDate(myDate0.getDate()-1);
            D0 = formatDate(myDate);
            myDate.setDate(myDate.getDate());
            D1 = formatDate(myDate);
            $('#d1').text(myDate.getDate());
            myDate.setDate(myDate.getDate()+1);
            D2 = formatDate(myDate);
            $('#d2').text(myDate.getDate());
            myDate.setDate(myDate.getDate()+1);
            D3 = formatDate(myDate);
            $('#d3').text(myDate.getDate());
            myDate.setDate(myDate.getDate()+1);
            D4 = formatDate(myDate);
            $('#d4').text(myDate.getDate());
            myDate.setDate(myDate.getDate()+1);
            D5 = formatDate(myDate);
            $('#d5').text(myDate.getDate());
            myDate.setDate(myDate.getDate()+1);
            D6 = formatDate(myDate);
            $('#d6').text(myDate.getDate());
            myDate.setDate(myDate.getDate()+1);
            D7 = formatDate(myDate);
            $('#d7').text(myDate.getDate());
            myDate.setDate(myDate.getDate()+1);
            D8 = formatDate(myDate);

            var startdate = new Date(dit);
            var enddate = new Date(D7);
            $('#periodcover').text("Period Covered: "+monthNames[startdate.getMonth()]+" "+startdate.getDate()+", "+startdate.getFullYear()+" - "+monthNames[enddate.getMonth()]+" "+enddate.getDate()+", "+enddate.getFullYear());
            data_table.init();
        }
        function formatDate(date) {
            var d = new Date(date),
                    month = '' + (d.getMonth() + 1),
                    day = '' + d.getDate(),
                    year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [year, month, day].join('-');
        }
        var data_table = {
            data : {},
            init : function() {
                var product_list_table = $('#gasoline_reports_table');
                product_list_table.DataTable({
                    serverSide: true,
                    autoWidth: false,
                    retrieve: true,
                    ajax: {
                        "url": '/gasoline_report/datatable',
                        data: {
                            dd0: D0,
                            dd1: D1,
                            dd2: D2,
                            dd3: D3,
                            dd4: D4,
                            dd5: D5,
                            dd6: D6,
                            dd7: D7,
                            dd8: D8
                        },
                        dataType: 'json'
                    },
                    columns: [
                        {data: 'product', name: 'product',orderable: false},
                        {data: 'beginning_balance', name: 'beginning_balance',orderable: false},
                        {data: 'additional_litres', name: 'additional_litres',orderable: false},
                        {data: 'total_litres', name: 'total_litres',orderable: false},
                        {data: 'd1', name: 'd1',orderable: false},
                        {data: 'd2', name: 'd2',orderable: false},
                        {data: 'd3', name: 'd3',orderable: false},
                        {data: 'd4', name: 'd4',orderable: false},
                        {data: 'd5', name: 'd5',orderable: false},
                        {data: 'd6', name: 'd6',orderable: false},
                        {data: 'd7', name: 'd7',orderable: false},
                        {data: 'total_sales', name: 'total_sales',orderable: false},
                        {data: 'ending_balance', name: 'ending_balance',orderable: false},
                    ],

                    "dom": 'lCfrtip',
                    "order": [],
                    "colVis": {
                        "buttonText": "Columns",
                        "overlayFade": 0,
                        "align": "right"
                    },
                    "language": {
                        "lengthMenu": '_MENU_ entries per page',
                        "search": 'Search:<i class="fa fa-search" style=""></i>',
                        "paginate": {
                            "previous": '<i class="fa fa-angle-left"></i>',
                            "next": '<i class="fa fa-angle-right"></i>'
                        }
                    }
                });

            }
        };
    </script>
@endsection
