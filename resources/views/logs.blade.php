@extends('layouts.dashboard')

@section('content')
    <div class="row clearfix">
        <!-- Task Info -->
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="header">
                    <h4>LOGS</h4>
                </div>
                <div class="body">
                    <table id="logs_table" class="display">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Category</th>
                            <th>Action</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>User</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


@endsection
@section('scripts')
    <script type="text/javascript">
        $(document).ready( function () {
            data_table.init();
        } );

        var data_table = {
            data : {},
            init : function() {
                var general_logs_table = $('#logs_table');
                general_logs_table.DataTable({
                    serverSide: true,
                    autoWidth: false,
                    ajax: {
                        "url": '/logs/datatable'
                    },
                    columns: [
                        {data: 'id', name: 'id',orderable: true},
                        {data: 'category', name: 'category',orderable: false},
                        {data: 'action', name: 'action',orderable: false},
                        {data: 'date', name: 'date',orderable: false},
                        {data: 'time', name: 'time',orderable: false},
                        {data: 'user', name: 'user',orderable: false},
                    ],

                    "dom": 'lCfrtip',
                    "order": [],
                    "colVis": {
                        "buttonText": "Columns",
                        "overlayFade": 0,
                        "align": "right"
                    },
                    "language": {
                        "lengthMenu": '_MENU_ entries per page',
                        "search": 'Search:<i class="fa fa-search" style=""></i>',
                        "paginate": {
                            "previous": '<i class="fa fa-angle-left"></i>',
                            "next": '<i class="fa fa-angle-right"></i>'
                        }
                    }
                });
            }
        };

    </script>
@endsection