<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Welcome To Laguda Admin</title>
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="{{asset('css/waves.min.css')}}" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{{asset('css/animate.min.css')}}" rel="stylesheet" />

    <link href="{{asset('css/bootstrap-material-datetimepicker.css')}}" rel="stylesheet">

    <link href="{{asset('css/bootstrap-select.css')}}" rel="stylesheet">

    {{--<!-- Morris Chart Css-->--}}
    {{--<link href="plugins/morrisjs/morris.css" rel="stylesheet" />--}}

    <!-- Custom Css -->
    <link href="{{asset('css/style-dashboard.css')}}" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="{{asset('css/all-themes.css')}}" rel="stylesheet" />

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
    @yield('stylesheets')
</head>

<body class="theme-green">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="preloader">
            <div class="spinner-layer pl-green">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>
        </div>
        <p>Please wait...</p>
    </div>
</div>
<!-- #END# Page Loader -->
<!-- Overlay For Sidebars -->
<div class="overlay"></div>
<!-- #END# Overlay For Sidebars -->
<!-- Search Bar -->
<div class="search-bar">
    <div class="search-icon">
        <i class="material-icons">search</i>
    </div>
    <input type="text" placeholder="START TYPING...">
    <div class="close-search">
        <i class="material-icons">close</i>
    </div>
</div>
<!-- #END# Search Bar -->
<!-- Top Bar -->
<nav class="navbar" style="min-height: 70px;">
    <div class="container-fluid">
        <div class="navbar-header">
            <h3 style="color: white; margin-left: 32px;">Laguda Gas Station</h3>
            <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
            <a href="javascript:void(0);" class="bars"></a>
        </div>
    </div>
</nav>
<!-- #Top Bar -->
<section>
    <!-- Left Sidebar -->
    @include('layouts.left-sidebar')
    <!-- #END# Left Sidebar -->
</section>

<section class="content">
    <div class="container-fluid">
        @yield('content')
    </div>
</section>

<!-- Jquery Core Js -->
<script src="{{asset('js/jquery.min.js')}}"></script>
{{--<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>--}}
<!-- Bootstrap Core Js -->
<script src="{{asset('js/bootstrap.min.js')}}"></script>

<!-- Select Plugin Js -->
<script src="{{asset('js/bootstrap-select.js')}}"></script>

<!-- Slimscroll Plugin Js -->
<script src="{{asset('js/jquery.slimscroll.js')}}"></script>

<!-- Waves Effect Plugin Js -->
<script src="{{asset('js/waves.min.js')}}"></script>

<script src="{{asset('js/moment.js')}}"></script>
<script src="{{asset('js/bootstrap-material-datetimepicker.js')}}"></script>
<!-- Custom Js -->

<script src="{{asset('js/index.js')}}"></script>
<script src="{{asset('js/admin.js')}}"></script>
<script src="{{asset('js/jquery.cookie.js')}}"></script>
<!-- Demo Js -->
{{--<script src="{{asset('js/demo.js')}}"></script>--}}

<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
@yield('scripts')
</body>

</html>
