<!-- Left Sidebar -->
<aside id="leftsidebar" class="sidebar">
    <!-- User Info -->
    <div class="user-info">
        <div class="image">
            <img src="{{asset('user.png')}}" width="48" height="48" alt="User" />
        </div>
        <div class="info-container">
            <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: #000000;"><b>{{auth()->user()->name}}</b></div>
            <div class="email" style="color: #000000;">{{auth()->user()->email}}</div>
            <div class="btn-group user-helper-dropdown">
                <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                <ul class="dropdown-menu pull-right">
                    {{--<li><a href="javascript:void(0);"><i class="material-icons">person</i>Profile</a></li>--}}
                    {{--<li role="separator" class="divider"></li>--}}
                    {{--<li><a href="javascript:void(0);"><i class="material-icons">group</i>Followers</a></li>--}}
                    {{--<li><a href="javascript:void(0);"><i class="material-icons">shopping_cart</i>Sales</a></li>--}}
                    {{--<li><a href="javascript:void(0);"><i class="material-icons">favorite</i>Likes</a></li>--}}
                    {{--<li role="separator" class="divider"></li>--}}
                    <li>
                        <a class="nav-link" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            <i class="material-icons">input</i>{{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- #User Info -->
    <!-- Menu -->
    <div class="menu">
        <ul class="list">
            <li class="header">MAIN NAVIGATION</li>
            @if(Route::currentRouteName() == 'home')
                <li class="active">
            @else
                <li>
            @endif
                <a href="{{route('home')}}">
                    <i class="material-icons">home</i>
                    <span>Home</span>
                </a>
            </li>
            @if(Route::currentRouteName() == 'gas_transaction')
                <li class="active">
            @else
                <li>
            @endif
                <a href="{{route('gas_transaction')}}">
                    <i class="material-icons">payment</i>
                    <span>Gasoline Transaction</span>
                </a>
                </li>
            @if(Route::currentRouteName() == 'sales_transaction')
                <li class="active">
            @else
                <li>
                    @endif
                    <a href="{{route('sales_transaction')}}">
                        <i class="material-icons">payment</i>
                        <span>Sales Transaction</span>
                    </a>
                </li>
            @if(Route::currentRouteName() == 'gasoline_inventory' || Route::currentRouteName() == 'sales_inventory')
                <li class="active">
            @else
                <li>
                    @endif
                    <a href="#Inventory" class="menu-toggle">
                        <i class="material-icons">widgets</i>
                        <span>Inventory</span>
                    </a>
                    <ul class="ml-menu">
                        @if(Route::currentRouteName() == 'gasoline_inventory')
                            <li class="active">
                        @else
                            <li>
                                @endif
                                <a href="{{route('gasoline_inventory')}}">Gasoline</a>
                            </li>
                            @if(Route::currentRouteName() == 'sales_inventory')
                                <li class="active">
                            @else
                                <li>
                                    @endif
                                    <a href="{{route('sales_inventory')}}">Sales</a>
                                </li>
                    </ul>
                </li>
            @if(Route::currentRouteName() == 'accounts_receivable')
                <li class="active">
            @else
                <li>
            @endif
                <a href="{{route('accounts_receivable')}}">
                    <i class="material-icons">payment</i>
                    <span>Accounts Receivable</span>
                </a>
            </li>
            @if(Route::currentRouteName() == 'gasoline_reports' || Route::currentRouteName() == 'sales_reports')
                <li class="active">
            @else
                <li>
            @endif
                <a href="#Reports" class="menu-toggle">
                    <i class="material-icons">swap_calls</i>
                    <span>Reports</span>
                </a>
                <ul class="ml-menu">
                    @if(Route::currentRouteName() == 'gasoline_reports')
                        <li class="active">
                    @else
                        <li>
                    @endif
                        <a href="{{route('gasoline_reports')}}">Gasoline</a>
                    </li>
                    @if(Route::currentRouteName() == 'sales_reports')
                        <li class="active">
                    @else
                        <li>
                    @endif
                        <a href="{{route('sales_reports')}}">Sales</a>
                    </li>
                </ul>
            </li>
            @if(Route::currentRouteName() == 'logs')
                <li class="active">
            @else
                <li>
            @endif
                <a href="{{route('logs')}}">
                    <i class="material-icons">assignment</i>
                    <span>Logs</span>
                </a>
            </li>

        </ul>
    </div>
    <!-- #Menu -->
</aside>
<!-- #END# Left Sidebar -->