@extends('layouts.dashboard')

@section('content')
    <div class="block-header">
        <h2>Gasoline Transaction</h2>
    </div>

    <div class="row clearfix">
        <!-- Task Info -->
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <div class="card">
                <div class="header">
                    <h5>Payment</h5>
                </div>
                <div class="body">
                <form class="form-horizontal" id="gas_transaction">
                    <fieldset>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <!-- Select Basic -->
                        <div class="col-sm-12">
                            <div class="form-group form-group-lg">
                                <div class="form-line">
                                    <select id="type" name="type" class="form-control show-tick">
                                        <option value="">--SELECT GASOLINE--</option>
                                        @foreach($gasolines as $gasoline)
                                        <option value="{{$gasoline->gasoline_type}}">{{$gasoline->gasoline_type}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!-- Text input-->
                        <div class="col-sm-12">
                            <div class="form-group form-float form-group-lg">
                                <div class="form-line">
                                    <input id="amount" name="amount" type="number" class="form-control" step=".01" min="0" required disabled>
                                    <label class="form-label">Amount</label>
                                </div>
                            </div>
                        </div>
                        <!-- Text input-->
                        <div class="col-sm-12">
                            <div class="form-group form-float form-group-lg">
                                <div class="form-line">
                                    <input id="litres" name="litres" type="text" value="0 Litre(s)" class="form-control input-md" disabled>
                                    <label class="form-label">Litres</label>
                                </div>
                            </div>
                        </div>

                        <!-- Select Basic -->
                        <div class="col-sm-12">
                            <div class="form-group form-group-lg">
                                <label>SELECT PAYMENT METHOD:</label>
                                <div class="form-line">
                                    <select id="method" class="form-control show-tick">
                                        <option value="Cash" onclick='$("#accounts").hide();'>Cash</option>
                                        <option value="Account" onclick='$("#accounts").show();'>Account</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group form-float form-group-lg">
                                <div class="form-line">
                                    <input id="receipt" name="receipt" type="text" class="form-control input-md">
                                    <label class="form-label">Delivery Receipt Number</label>
                                </div>
                            </div>
                        </div>

                        <!-- Select Basic -->
                        <div class="col-sm-12" id="accounts">
                            <div class="form-group form-group-lg">
                                <label>SELECT ACCOUNT:</label>
                                <div class="form-line">
                                    <select id="account" class="form-control show-tick" disabled>
                                        @foreach($customers as $customer)
                                        <option value="{{$customer->name}}">{{$customer->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <!-- Button (Double) -->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="button1id"></label>
                            <div class="col-md-8">
                                <button type="button" id="submit" class="btn btn-success" disabled>Submit</button>
                                <button class="btn btn-default" type="reset">Clear</button>
                            </div>
                        </div>

                    </fieldset>
                </form>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <div class="card">
                <div class="header">
                    <h5>Summary</h5>
                </div>
                <div class="body">
                    <div class="form-group form-float form-group-lg">
                        <label class="form-label">Gasoline Type:</label>
                        <label class="form-label" id="gasoline_type_label"> </label>
                    </div>
                    <div class="form-group form-float form-group-lg">
                        <label class="form-label">Amount:</label>
                        <label class="form-label" id="amount_label"> </label>
                    </div>
                    <div class="form-group form-float form-group-lg">
                        <label class="form-label">Litres:</label>
                        <label class="form-label" id="litres_label"> </label>
                    </div>
                    <div class="form-group form-float form-group-lg">
                        <label class="form-label">Payment Method:</label>
                        <label class="form-label" id="payment_method_label"> </label>
                    </div>
                    <div class="form-group form-float form-group-lg">
                        <label class="form-label">Delivery Receipt Number:</label>
                        <label class="form-label" id="receipt_label"> </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $("#type").change(function () {
            if($("#type").val() != ''){
                $('#gas_transaction input[name=amount]').val('');
                $('#gas_transaction input[name=amount]').prop('disabled', false);
            }
            else{
                $('#gas_transaction input[name=amount]').prop('disabled', true);
            }
        });

        $('#gas_transaction input[name=amount]').keyup(function() {
            if($('#gas_transaction input[name=amount]').val() == ''){
                $('#gas_transaction #submit').prop('disabled', true);
            }

            else{
                $('#gas_transaction #submit').prop('disabled', false);
            }

            var gasoline_type = $('#type').val();
            var amount = $('#gas_transaction input[name=amount]').val();
            $.ajax({
                url: '/gas_transaction/get_litres',
                method: 'POST',
                data: {
                    _token: $('#gas_transaction input[name=_token]').val(),
                    type: gasoline_type,
                    amount: amount
                },
                dataType: 'json',
                success: function(data){
                    $('#gas_transaction input[name=litres]').attr('value',data.litres);
                },
                error: function (data) {
                    console.log(data);
                }
            });
        });

        $("#method").change(function () {

            if($("#method").val() != 'Account'){
                $('#gas_transaction #account').prop('disabled', true);
            }
            else{
                $('#gas_transaction #account').prop('disabled', false);
                if($('#gas_transaction input[name=amount]').val() == ''){
                    $('#gas_transaction #submit').prop('disabled', true);
                }

                else{
                    $('#gas_transaction #submit').prop('disabled', false);
                }
            }
        });

        $('#gas_transaction #submit').click(function(){
            if(confirm('Are you sure you want to make this transaction?')){
                var gasoline_type = $('#type').val();
                var amount = $('#gas_transaction input[name=amount]').val();
                var litres = $('#gas_transaction input[name=litres]').val();
                var method = $('#method').val();
                if($('#gas_transaction input[name=receipt]').val() == ''){
                    var receipt = 'N/A';
                }
                else{
                    var receipt = $('#gas_transaction input[name=receipt]').val();
                }
                var account = $('#account').val();

                $.ajax({
                    url: '/gas_transaction/save_transaction',
                    method: 'POST',
                    data: {
                        _token: $('#gas_transaction input[name=_token]').val(),
                        type: gasoline_type,
                        amount: amount,
                        litres: litres,
                        payment: method,
                        receipt: receipt,
                        account: account
                    },
                    dataType: 'json',
                    success: function(data){
                        alert(data.message);
                        $('#gasoline_type_label').text(gasoline_type);
                        $('#amount_label').text(amount);
                        $('#litres_label').text(litres);
                        $('#payment_method_label').text(method);
                        $('#receipt_label').text(receipt);
                    },
                    error: function (data) {
                        console.log(data);
                    }
                });
            }
            else{
                alert('Canceled');
            }
        });
    </script>
@endsection
