@extends('layouts.dashboard')

@section('stylesheets')
    <link href="{{asset('css/bootstrap-material-datetimepicker.css')}}" rel="stylesheet">
@endsection

@section('content')
    <div class="block-header">
        <h2>Gasoline Inventory</h2>
    </div>

    <div class="row clearfix">
        <!-- Task Info -->
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <form method="GET" action="{{route('gasoline-inventory-csv')}}" class="pull-right">
                        <button class="btn btn-success" type="submit">Download CSV</button>
                    </form>
                    <h4>LAGUDA GAS STATION</h4>
                    <h5>Cor. Broce / S. Carmona Street, San Carlos City , Negros Occidental 6127</h5>
                    <h5>Date: {{Carbon\Carbon::today('Asia/Manila')->format('M. d, Y')}}</h5>
                    <div class="row clearfix">

                    </div>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table id="gasoline_inventory_table" class="table table-hover dashboard-task-infos">
                            <thead>
                            <tr>
                                <th>Type</th>
                                <th>Price per Litre (in Peso)</th>
                                <th>Current Balance (Litres)</th>
                                <th>Actions</th>
                            </tr>
                            </thead>

                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>



        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h4>Gasoline Transaction History</h4>
                    <div class="row clearfix">

                    </div>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table id="gasoline_inventory_logs_table" class="display">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Date & Time</th>
                                    <th>Product</th>
                                    <th>Transaction</th>
                                    <th>Litres</th>
                                    <th>Previous Litres</th>
                                    <th>New Balance</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Update Price</h5>
                    </div>
                    <div class="modal-body">
                        <form method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="col-sm-12">
                            <div class="form-group form-float">
                                <div class="form-line disabled">
                                    <input type="text" class="form-control" id="gasoline_type" disabled />
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" id="gasoline_price" value="{{ old('first_name') }}">
                                    <label class="form-label">Price per Litre</label>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="save_price">Save changes</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Add Gasoline</h5>
                    </div>
                    <div class="modal-body">
                        <form method="POST">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="col-sm-12">
                                <div class="form-group form-float">
                                    <div class="form-line disabled">
                                        <input type="text" class="form-control" id="gasoline_type" disabled />
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group form-float">
                                    <div class="form-line disabled">
                                        <input type="text" class="form-control" id="current_balance" disabled />
                                        <label class="form-label">Current Balance (Litres)</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group form-float">
                                    <div class="form-line focused">
                                        <input type="number" value="0" step=".01" min=".01" class="form-control" id="added_litres">
                                        <label class="form-label">Added Litres</label>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="add_gasoline">Add Litres</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function (){
            data_table.init();
            data_table2.init();
        });

        var data_table = {
            data : {},
            init : function() {
                var gasoline_inventory_table = $('#gasoline_inventory_table');
                gasoline_inventory_table.DataTable({
                    serverSide: true,
                    autoWidth: false,
                    ajax: {
                        "url": '/gasoline_inventory/datatable'
                    },
                    columns: [
                        {data: 'product', name: 'product',orderable: false},
                        {data: 'price', name: 'price',orderable: false},
                        {data: 'balance', name: 'balance',orderable: false},
                        {data: 'actions', name: 'actions',orderable: false},
                    ],

                    "dom": 'lCfrtip',
                    "order": [],
                    "colVis": {
                        "buttonText": "Columns",
                        "overlayFade": 0,
                        "align": "right"
                    },
                    "language": {
                        "lengthMenu": '_MENU_ entries per page',
                        "search": 'Search:<i class="fa fa-search" style=""></i>',
                        "paginate": {
                            "previous": '<i class="fa fa-angle-left"></i>',
                            "next": '<i class="fa fa-angle-right"></i>'
                        }
                    },
                    fnDrawCallback : function(){
                        $('.update_price').click(function(){
                            $('#updateModal').find('form').trigger('reset');
                            var type = $(this).data('type');
                            var price = $(this).data('price');
                            $('#updateModal').modal({
                                show: 'true'
                            });
                            $('#updateModal #gasoline_type').attr('value',type);
                            $('#updateModal #gasoline_price').parent().addClass("focused");
                            $('#updateModal #gasoline_price').attr('value',price);
                            $('#updateModal #save_price').prop('disabled', true);
                            $('#updateModal #gasoline_price').keyup(function() {
                                if($(this).val() != price && $(this).val() != '') {
                                    $('#updateModal #save_price').prop('disabled', false);
                                }
                                else{
                                    $('#updateModal #save_price').prop('disabled', true);
                                }
                            });
                        });

                        $('#updateModal #save_price').one("click", function() {
                            $('#updateModal').modal('hide');
                            var type = $('#updateModal #gasoline_type').val();
                            var price = $('#updateModal #gasoline_price').val();
                            $.ajax({
                                url: '/inventory/gasoline/save',
                                method: 'POST',
                                data: {
                                    _token: $('#updateModal input[name=_token]').val(),
                                    type: type,
                                    price: price
                                },
                                dataType: 'json',
                                success: function(data){
                                    alert(data.message);
                                },
                                error: function (data) {
                                    console.log(data);
                                },
                                complete: function(){
                                    $('#gasoline_inventory_table').dataTable().fnDraw();
                                }
                            });
                        });

                        $('.add_litres').click(function(){
                            $('#addModal').find('form').trigger('reset');
                            var type = $(this).data('type');
                            var balance = $(this).data('balance');
                            $('#addModal').modal({
                                show: 'true'
                            });
                            $('#addModal #gasoline_type').attr('value',type);
                            $('#addModal #current_balance').parent().addClass("focused");
                            $('#addModal #current_balance').attr('value',balance);
                            $('#addModal #add_gasoline').prop('disabled', true);
                            $('#addModal #added_litres').keyup(function() {
                                if($(this).val() < 0.01) {
                                    $('#addModal #add_gasoline').prop('disabled', true);
                                }
                                else{
                                    $('#addModal #add_gasoline').prop('disabled', false);
                                }
                            });
                        });

                        $('#addModal #add_gasoline').one("click", function() {
                            $('#addModal').modal('hide');
                            var type = $('#addModal #gasoline_type').val();
                            var balance = $('#addModal #current_balance').val();
                            var litres = $('#addModal #added_litres').val();
                            $.ajax({
                                url: '/inventory/gasoline/add',
                                method: 'POST',
                                data: {
                                    _token: $('#updateModal input[name=_token]').val(),
                                    type: type,
                                    balance: balance,
                                    litres: litres
                                },
                                dataType: 'json',
                                success: function(data){
                                    alert(data.message);
                                },
                                error: function (data) {
                                    console.log(data);
                                },
                                complete: function(){
                                    $('#gasoline_inventory_table').dataTable().fnDraw();
                                    $('#gasoline_inventory_logs_table').dataTable().fnDraw();
                                }
                            });
                        });
                    }
                });
            }
        };

        var data_table2 = {
            data : {},
            init : function() {
                var gasoline_transactions_table = $('#gasoline_inventory_logs_table');
                gasoline_transactions_table.DataTable({
                    serverSide: true,
                    autoWidth: false,
                    ajax: {
                        "url": '/gasoline_transactions/datatable'
                    },
                    columns: [
                        {data: 'id', name: 'id',orderable: true},
                        {data: 'date', name: 'date',orderable: true},
                        {data: 'product', name: 'product',orderable: false},
                        {data: 'transaction', name: 'transaction',orderable: false},
                        {data: 'litres', name: 'litres',orderable: false},
                        {data: 'previous_litres', name: 'previous_litres',orderable: false},
                        {data: 'new_balance', name: 'new_balance',orderable: false},
                    ],

                    "dom": 'lCfrtip',
                    "order": [],
                    "colVis": {
                        "buttonText": "Columns",
                        "overlayFade": 0,
                        "align": "right"
                    },
                    "language": {
                        "lengthMenu": '_MENU_ entries per page',
                        "search": 'Search:<i class="fa fa-search" style=""></i>',
                        "paginate": {
                            "previous": '<i class="fa fa-angle-left"></i>',
                            "next": '<i class="fa fa-angle-right"></i>'
                        }
                    }
                });
            }
        };

    </script>
@endsection
