@extends('layouts.dashboard')

@section('content')
    <div class="block-header">
        <h2>DASHBOARD</h2>
    </div>
    
    <div class="row clearfix">

        <!-- Visitors -->
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <a href="{{route('gasoline_inventory')}}" style="text-decoration: none">
                <div class="card">
                    <div class="body bg-pink">
                        <div class="font-bold m-b--35">GASOLINE REPORT TODAY</div>
                        <ul class="dashboard-stat-list">
                            @foreach($gas_reports as $gas_report)
                            <li>
                                {{$gas_report->gasoline_type}}
                                <span class="pull-right"><b>{{$gas_report->current_balance}}</b> <small>Litres</small></span>
                            </li>
                            @endforeach
                    </div>
                </div>
            </a>
        </div>

        <!-- #END# Visitors -->

        <!-- #END# Latest Social Trends -->
        <!-- Answered Tickets -->
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <a href="{{route('accounts_receivable')}}" style="text-decoration: none">
            <div class="card">
                <div class="body bg-teal">
                    <div class="font-bold m-b--35">ACCOUNTS RECEIVABLE REPORT TODAY</div>
                    <ul class="dashboard-stat-list">
                        @foreach($receivables as $receivable)
                        <li>
                            {{$receivable->account}}
                            <span class="pull-right"><b>{{$receivable->total_charge}} </b><small>Pesos</small></span>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            </a>
        </div>
        <!-- #END# Answered Tickets -->
    </div>

    <div class="row clearfix">
        <!-- Task Info -->
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="header">
                    <h2>LOGS</h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="javascript:void(0);">Action</a></li>
                                <li><a href="javascript:void(0);">Another action</a></li>
                                <li><a href="javascript:void(0);">Something else here</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <a href="{{route('logs')}}" style="text-decoration: none">
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-hover dashboard-task-infos">
                                <thead>
                                <tr>
                                    <th>Category</th>
                                    <th>Action</th>
                                    <th>Date</th>
                                    <th>Time</th>
                                    <th>User</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($logs as $log)
                                <tr>
                                    <td>{{$log->category}}</td>
                                    <td>{{$log->action}}</td>
                                    <td>{{$log->created_at->format('m/d/Y')}}</td>
                                    <td>{{$log->created_at->format('g:i A')}}</td>
                                    <td>{{$log->user}}</td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>

@endsection
