@extends('layouts.dashboard')


@section('content')
    <div class="block-header">
        <h2>Sales Inventory</h2>
    </div>

    <!-- Modal -->
    <div id="Add_Sales_Item_modal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <form class="form-horizontal">
                    <fieldset>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Add Sales Inventory Item</h4>
                        </div>
                        <div class="modal-body">
                            <!-- Text input-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="textinput">Product Name</label>
                                <div class="col-md-6">
                                    <input id="product_name" name="product_name" type="text" placeholder="Product Name" class="form-control input-md" required="">
                                </div>
                            </div>

                            <!-- Text input-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="textinput">Price</label>
                                <div class="col-md-6">
                                    <input id="price" name="price" type="number" placeholder="0.00" class="form-control input-md" required="" step=".01" min="0">
                                </div>
                            </div>

                            <!-- Text input-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="textinput">Quantity</label>
                                <div class="col-md-6">
                                    <input id="quantity" name="quantity" type="number" placeholder="0" class="form-control input-md" required=""  min="0">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <!-- Button (Double) -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="button1id"></label>
                                <div class="col-md-8">
                                    <button id="add_sales_item" name="button1id" class="btn btn-success" type="submit">Add</button>
                                    <button id="button2id" name="button2id" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>

        </div>
    </div>
    <div class="modal fade" id="salesUpdateModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Update Price</h5>
                </div>
                <div class="modal-body">
                    <form method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="col-sm-12">
                            <div class="form-group form-float">
                                <div class="form-line disabled">
                                    <input type="text" class="form-control" id="product" disabled />
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" id="sales_price" value="{{ old('first_name') }}">
                                    <label class="form-label">Price</label>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="save_price">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="salesAddModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Quantity</h5>
                </div>
                <div class="modal-body">
                    <form method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="col-sm-12">
                            <div class="form-group form-float">
                                <div class="form-line disabled">
                                    <input type="text" class="form-control" id="product" disabled />
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group form-float">
                                <div class="form-line disabled">
                                    <input type="text" class="form-control" id="current_balance" disabled />
                                    <label class="form-label">Current Balance (Quantity)</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group form-float">
                                <div class="form-line focused">
                                    <input type="number" value="0" step=".01" min=".01" class="form-control" id="added_quantity">
                                    <label class="form-label">Added Quantity</label>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="add_quantity">Add Quantity</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row clearfix">
        <!-- Task Info -->
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <form method="GET" action="{{route('sales-inventory-csv')}}" class="pull-right">
                        <button class="btn btn-success" type="submit">Download CSV</button>
                    </form>
                    <div class="pull-right">
                        <button type="button" class="btn btn-info btn-lg Add_Sales_Item">Add</button>
                    </div>
                    <h4>LAGUDA GAS STATION</h4>
                    <h5>Cor. Broce / S. Carmona Street, San Carlos City , Negros Occidental 6127</h5>
                    <h5>Date: {{Carbon\Carbon::today()->format('M. d, Y')}}</h5>
                    <div class="row clearfix">

                    </div>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table id="sales_inventory_table" class="display">
                            <thead>
                            <tr>
                                <th>Barcode</th>
                                <th>Product</th>
                                <th>Price (in Peso)</th>
                                <th>Balance (Quantity)</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h4>Sales Transaction History</h4>
                    <div class="row clearfix">

                    </div>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table id="sales_inventory_logs_table" class="display">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Date & Time</th>
                                <th>Product</th>
                                <th>Transaction</th>
                                <th>Qty.</th>
                                <th>Previous Qty. Balance</th>
                                <th>New Qty. Balance</th>
                                <th>User</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

    <script type="text/javascript">
        $(document).ready(function (){
            data_table.init();
            data_logs_table.init();
//            $('#sales_inventory_logs_table').DataTable();
//            $('#sales_inventory_table').DataTable();


        });

        var data_table = {
            data : {},
            init : function() {
                var sales_inventory_table = $('#sales_inventory_table');
                sales_inventory_table.DataTable({
                    serverSide: true,
                    autoWidth: false,
                    ajax: {
                        "url": '/sales_inventory/datatable'
                    },
                    columns: [
                        {data: 'barcode', name: 'barcode',orderable: false},
                        {data: 'product', name: 'product',orderable: false},
                        {data: 'price', name: 'price',orderable: false},
                        {data: 'balance', name: 'balance',orderable: false},
                        {data: 'actions', name: 'actions',orderable: false},
                    ],

                    "dom": 'lCfrtip',
                    "order": [],
                    "colVis": {
                        "buttonText": "Columns",
                        "overlayFade": 0,
                        "align": "right"
                    },
                    "language": {
                        "lengthMenu": '_MENU_ entries per page',
                        "search": 'Search:<i class="fa fa-search" style=""></i>',
                        "paginate": {
                            "previous": '<i class="fa fa-angle-left"></i>',
                            "next": '<i class="fa fa-angle-right"></i>'
                        }
                    },
                    fnDrawCallback : function(){
                        $('.sales_update_price').click(function(){
                            $('#salesUpdateModal').find('form').trigger('reset');
                            var product = $(this).data('product');
                            var price = $(this).data('price');
                            $('#salesUpdateModal').modal({
                                show: 'true'
                            });
                            $('#salesUpdateModal #product').attr('value',product);
                            $('#salesUpdateModal #sales_price').parent().addClass("focused");
                            $('#salesUpdateModal #sales_price').attr('value',price);
                            $('#salesUpdateModal #save_price').prop('disabled', true);
                            $('#salesUpdateModal #sales_price').keyup(function() {
                                if($(this).val() != price && $(this).val() != '') {
                                    $('#salesUpdateModal #save_price').prop('disabled', false);
                                }
                                else{
                                    $('#salesUpdateModal #save_price').prop('disabled', true);
                                }
                            });
                        });

                        $('#salesUpdateModal #save_price').one("click", function() {
                            $('#salesUpdateModal').modal('hide');
                            var product = $('#salesUpdateModal #product').val();
                            var price = $('#salesUpdateModal #sales_price').val();
                            $.ajax({
                                url: '/inventory/sales/save',
                                method: 'POST',
                                data: {
                                    _token: $('#salesUpdateModal input[name=_token]').val(),
                                    product: product,
                                    price: price
                                },
                                dataType: 'json',
                                success: function(data){
                                    alert(data.message);
                                },
                                error: function (data) {
                                    console.log(data);
                                },
                                complete: function(){
                                    $('#sales_inventory_table').dataTable().fnDraw();
                                    $('#sales_inventory_logs_table').dataTable().fnDraw();
                                    location.reload(true)
                                }
                            });
                        });

                        $('.sales_add_quantity').click(function(){
                            $('#salesAddModal').find('form').trigger('reset');
                            var product = $(this).data('product');
                            var balance = $(this).data('balance');
                            $('#salesAddModal').modal({
                                show: 'true'
                            });
                            $('#salesAddModal #product').attr('value',product);
                            $('#salesAddModal #current_balance').parent().addClass("focused");
                            $('#salesAddModal #current_balance').attr('value',balance);
                            $('#salesAddModal #add_quantity').prop('disabled', true);
                            $('#salesAddModal #added_quantity').keyup(function() {
                                if($(this).val() < 0.01) {
                                    $('#salesAddModal #add_quantity').prop('disabled', true);
                                }
                                else{
                                    $('#salesAddModal #add_quantity').prop('disabled', false);
                                }
                            });
                        });

                        $('#salesAddModal #add_quantity').click(function(){
                            $('#salesAddModal').modal('hide');
                            var curr_product = $('#salesAddModal #product').val();
                            var curr_balance = $('#salesAddModal #current_balance').val();
                            var added_quantity = $('#salesAddModal #added_quantity').val();
                            $.ajax({
                                url: '/inventory/sales/add_quantity',
                                method: 'POST',
                                data: {
                                    _token: $('#salesAddModal input[name=_token]').val(),
                                    product: curr_product,
                                    balance: curr_balance,
                                    quantity: added_quantity
                                },
                                dataType: 'json',
                                success: function(data){
                                    alert(data.message);
                                },
                                error: function (data) {
                                    console.log(data);
                                },
                                complete: function(){
                                    $('#sales_inventory_table').dataTable().fnDraw();
                                    $('#sales_inventory_logs_table').dataTable().fnDraw();
                                    location.reload(true)
                                }
                            });
                        });


                        $('.Add_Sales_Item').click(function(){
                            $('#Add_Sales_Item_modal').find('form').trigger('reset');
                            $('#Add_Sales_Item_modal').modal({
                                show: 'true'
                            });

                        });

                        $('#Add_Sales_Item_modal #add_sales_item').one("click", function() {
                            $('#Add_Sales_Item_modal').modal('hide');
                            var add_product = $('#Add_Sales_Item_modal #product_name').val();
                            var add_price = $('#Add_Sales_Item_modal #price').val();
                            var add_quantity = $('#Add_Sales_Item_modal #quantity').val();
                            $.ajax({
                                url: '/inventory/sales/add',
                                method: 'POST',
                                data: {
                                    _token: $('#Add_Sales_Item_modal input[name=_token]').val(),
                                    product: add_product,
                                    price: add_price,
                                    quantity: add_quantity
                                },
                                dataType: 'json',
                                success: function(data){
                                    alert(data.message);
                                },
                                error: function (data) {
                                    console.log(data);
                                },
                                complete: function(){
                                    $('#sales_inventory_table').dataTable().fnDraw();
                                    $('#sales_inventory_logs_table').dataTable().fnDraw();
                                    location.reload(true)
                                }
                            });
                        });
                    }
                });
            }
        };

        var data_logs_table = {
            data : {},
            init : function() {
                var sales_inventory_logs_table = $('#sales_inventory_logs_table');
                sales_inventory_logs_table.DataTable({
                    serverSide: true,
                    autoWidth: false,
                    ajax: {
                        "url": '/sales_inventory/datatable/logs'
                    },
                    columns: [
                        {data: 'id', name: 'id',orderable: false},
                        {data: 'datetime', name: 'datetime',orderable: false},
                        {data: 'product', name: 'product',orderable: false},
                        {data: 'transaction', name: 'transaction',orderable: false},
                        {data: 'quantity', name: 'quantity',orderable: false},
                        {data: 'prev_quantity', name: 'prev_quantity',orderable: false},
                        {data: 'new_quantity', name: 'new_quantity',orderable: false},
                        {data: 'user', name: 'user',orderable: false},
                    ],

                    "dom": 'lCfrtip',
                    "order": [],
                    "colVis": {
                        "buttonText": "Columns",
                        "overlayFade": 0,
                        "align": "right"
                    },
                    "language": {
                        "lengthMenu": '_MENU_ entries per page',
                        "search": 'Search:<i class="fa fa-search" style=""></i>',
                        "paginate": {
                            "previous": '<i class="fa fa-angle-left"></i>',
                            "next": '<i class="fa fa-angle-right"></i>'
                        }
                    }
                });
            }
        };

    </script>
@endsection
