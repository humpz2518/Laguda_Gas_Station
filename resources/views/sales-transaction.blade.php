@extends('layouts.dashboard')

@section('content')
    <div class="block-header">
        <h2>Sales Transaction</h2>
    </div>
    <div class="row clearfix">
        <!-- Task Info -->
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <div class="card">
                <div class="header">
                    <h5>Product</h5>
                </div>
                <div class="body">
                    <table id="product_list_table" class="display">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Item</th>
                            <th>Price</th>
                            <th>Quantity</th>
                        </tr>
                        </thead>
                        <tbody>
                        {{--<tr>--}}
                            {{--<td>12191991</td>--}}
                            {{--<td>Lubricant</td>--}}
                            {{--<td>P 150.00</td>--}}
                            {{--<td>--}}
                                {{--<input style="width: 50px;" type="number" min="0" size="4" value="0"/>--}}
                                {{--<button>Add</button>--}}
                            {{--</td>--}}
                        {{--</tr>--}}
                        {{--<tr>--}}
                            {{--<td>12121311</td>--}}
                            {{--<td>hydro fork oil</td>--}}
                            {{--<td>P 300.00</td>--}}
                            {{--<td>--}}
                                {{--<input style="width: 50px;" type="number" min="0" size="4" value="0"/>--}}
                                {{--<button>Add</button>--}}
                            {{--</td>--}}
                        {{--</tr>--}}
                        {{--<tr>--}}
                            {{--<td>42112991</td>--}}
                            {{--<td>SUPER COOLANT</td>--}}
                            {{--<td>P 50.00</td>--}}
                            {{--<td>--}}
                                {{--<input style="width: 50px;" type="number" min="0" size="4" value="0"/>--}}
                                {{--<button>Add</button>--}}
                            {{--</td>--}}
                        {{--</tr>--}}
                        {{--<tr>--}}
                            {{--<td>16961991</td>--}}
                            {{--<td>Aero Lube 1000ml</td>--}}
                            {{--<td>P 3000.00</td>--}}
                            {{--<td>--}}
                                {{--<input style="width: 50px;" type="number" min="0" size="4" value="0"/>--}}
                                {{--<button>Add</button>--}}
                            {{--</td>--}}
                        {{--</tr>--}}
                        {{--<tr>--}}
                            {{--<td>09091991</td>--}}
                            {{--<td>Aero Lube 250ml</td>--}}
                            {{--<td>P 10.00</td>--}}
                            {{--<td>--}}
                                {{--<input style="width: 50px;" type="number" min="0" size="4" value="0"/>--}}
                                {{--<button>Add</button>--}}
                            {{--</td>--}}
                        {{--</tr>--}}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <div class="card">
                <div class="header">
                    <h4>LAGUDA GAS STATION</h4>
                    <h5>Cor. Broce / S. Carmona Street, San Carlos City , Negros Occidental 6127</h5>
                    <h5>Date: August 25, 2018</h5>
                </div>
                <div class="body">


                            <div class="table-responsive">
                                <table class="table table-hover dashboard-task-infos" id="sales_buy_table">
                                    <thead>
                                    <tr>
                                        <th>Item</th>
                                        <th>Price (&#8369;)</th>
                                        <th>Qty.</th>
                                        <th>Sub Total</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    {{--<tr>--}}
                                        {{--<td>Oil</td>--}}
                                        {{--<td>300.00</td>--}}
                                        {{--<td>2</td>--}}
                                        {{--<td>600.00</td>--}}
                                        {{--<td><i class="material-icons" style="color: #ff3111;">clear</i></td>--}}
                                    {{--</tr>--}}
                                    {{--<tr>--}}
                                        {{--<td>Lubricant</td>--}}
                                        {{--<td>150.00</td>--}}
                                        {{--<td>1</td>--}}
                                        {{--<td>150.00 </td>--}}
                                        {{--<td><i class="material-icons" style="color: #ff3111;">clear</i></td>--}}
                                    {{--</tr>--}}

                                    {{--<tr>--}}
                                        {{--<td>Aero Lube 1000ml</td>--}}
                                        {{--<td>3000.00</td>--}}
                                        {{--<td>1</td>--}}
                                        {{--<td>3000.00</td>--}}
                                        {{--<td><i class="material-icons" style="color: #ff3111;">clear</i></td>--}}
                                    {{--</tr>--}}
                                    </tbody>
                                </table>
                            </div>
                            <!-- Text input-->
                    <form class="form-horizontal" method="post">
                        <fieldset>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="checkboxes"></label>
                                <div class="col-md-4">
                                    <div class="checkbox">
                                        <label for="checkboxes-0">
                                            <input type="checkbox" name="checkboxes" id="methoding" onclick="toggle(this)" value="1">
                                            Account Payment?
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group pull-right" id="toktoken">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <label class="col-md-8 control-label" for="textinput">TOTAL: &#8369;</label>
                                <div class="col-md-3">
                                    <input id="subtotal" name="textinput" type="text" placeholder="" value="0.00" class="form-control input-md" disabled>

                                </div>
                            </div>

                            <!-- Text input-->
                            <div class="form-group pull-right cashing">
                                <label class="col-md-8 control-label" for="textinput">Cash: &#8369;</label>
                                <div class="col-md-3">
                                    <input id="cash" name="number" type="number" placeholder="0.00" class="form-control input-md" required="" step=".01" min="0">

                                </div>
                            </div>

                            <!-- Text input-->
                            <div class="form-group pull-righ chinging">
                                <label class="col-md-8 control-label" for="textinput">Change: &#8369;</label>
                                <div class="col-md-3">
                                    <input id="total" name="textinput" type="text" placeholder="" value="" class="form-control input-md"  disabled>

                                </div>
                            </div>

                            <div class="col-sm-12 accounts">
                                <div class="form-group form-float form-group-lg">
                                    <div class="form-line">
                                        <input id="receipt" name="receipt" type="text" class="form-control input-md">
                                        <label class="form-label">Delivery Receipt Number</label>
                                    </div>
                                </div>
                            </div>

                            <!-- Select Basic -->
                            <div class="col-sm-12 accounts">
                                <div class="form-group">
                                    <div class="form-line">
                                        <select id="accounts" name="selectbasic" class="form-control show-tick">
                                            <option value="">--SELECT ACCOUNT--</option>
                                            @foreach($customers as $customer)
                                                <option value="{{$customer->name}}">{{$customer->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <!-- Button (Double) -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="button1id"></label>
                                <div class="col-md-8">
                                    <button id="submitbtn" type="button" onclick="verifysubmit('sales_buy_table')" name="button1id" class="btn btn-success subsub">Submit</button>
                                    <button id="button2id" name="button2id" class="btn btn-warning" type="button" onclick="location.reload(true)">Clear</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>


@endsection
@section('scripts')

    <script type="text/javascript">
        var gcount = 0;

        $(document).ready(function (){
            $(".accounts").hide();
            $('#methoding').val(0);
            gcount = 0;
            data_table.init();

            var total = $('#cash').val() - $('#subtotal').val();
            $('#total').val(total)
            $('#cash').keyup(function(){
                total = $('#cash').val() - $('#subtotal').val();
                $('#total').val(total)
            });

//            $('input[type="checkbox"]').on('change', function() {
//                $(this).closest('div').find('.cashing').toggle(!this.checked);
//                $(this).closest('div').find('.chinging').toggle(!this.checked);
//                $(this).closest('div').find('.accounts').toggle(!this.checked);
//            });
            checking


        });
//        function checkings(){
//            if(('#checking').is(':checked')){
//                $('.cashing').hide();
//                $('.chinging').hide();
//            }else{
//                $('.accounts').show();
//            }
//        }
        function toggle(obj) {
            if ( obj.checked ){
                $('.cashing').hide();
                $('.chinging').hide();
                $('.accounts').show();
                $('#methoding').val(1);
//                alert($('#methoding').val());
            }
            else{
                $('.cashing').show();
                $('.chinging').show();
                $('.accounts').hide();
                $('#methoding').val(0);
//                alert($('#methoding').val());

            }
        }
        var data_table = {
            data : {},
            init : function() {
                var product_list_table = $('#product_list_table');
                product_list_table.DataTable({
                    serverSide: true,
                    autoWidth: false,
                    ajax: {
                        "url": '/sales_transaction/datatable'
                    },
                    columns: [
                        {data: 'id', name: 'id',orderable: false},
                        {data: 'item', name: 'item',orderable: false},
                        {data: 'price', name: 'price',orderable: false},
                        {data: 'quantity', name: 'quantity',orderable: false},
                    ],

                    "dom": 'lCfrtip',
                    "order": [],
                    "colVis": {
                        "buttonText": "Columns",
                        "overlayFade": 0,
                        "align": "right"
                    },
                    "language": {
                        "lengthMenu": '_MENU_ entries per page',
                        "search": 'Search:<i class="fa fa-search" style=""></i>',
                        "paginate": {
                            "previous": '<i class="fa fa-angle-left"></i>',
                            "next": '<i class="fa fa-angle-right"></i>'
                        }
                    },
                    fnDrawCallback : function(){
                        $('.sales_add_to_list').click(function(){
//                            alert("product and qty");
                            gcount++;
                            var product = $(this).data('product');
                            var price = $(this).data('price');
                            var qtyid = $(this).data('qty');
                            var bal = $(this).data('balance');
                            var qty = $("#"+qtyid).val();
                            var subtotals = price * qty;
                            if(bal == 0){
                                alert("0 balance for Item:"+product);
                            }else{
                                if(qty > 0){
                                    if(qty > bal){
                                        alert("Insufficient balance for Item:"+product+". Current balance is "+bal);
                                    }else{
                                        alert("Added Product: "+product+" and Quantity: "+qty);
                                        $('#sales_buy_table').find('tbody')
                                                .append($('<tr>')
                                                        .attr('id',gcount)
                                                        .append($('<td>')
                                                                .text(product)
                                                )
                                                        .append($('<td>')
                                                                .text(price)
                                                )
                                                        .append($('<td>')
                                                                .text(qty)
                                                )
                                                        .append($('<td>')
                                                                .attr('class','getsubtotal')
                                                                .text(subtotals)
                                                )
                                                        .append($('<td>')
                                                                .append($('<button>')
                                                                        .attr('data-targ',gcount)
                                                                        .attr('type','button')
//                                                        .attr('id','remlistrow')
                                                                        .attr('class','material-icons remm remlistrow')
                                                                        .attr('onclick','clickeringI('+gcount+')')
                                                                        .attr('style','color: #ff3111;')
                                                                        .text('clear')
                                                        )
                                                )
                                        );
                                        getsubtototal();
                                    }
                                }else{
                                    alert("Please enter Quantity.")
                                }
                            }

//                            var iddd  = $(this).uniqueId();
                                        // $(


                        });


                    }
                });

            }
        };
        function clickeringI(gcgc){
            alert('Removed item number '+gcgc+'.');
            $("#sales_buy_table #"+gcgc).remove();
            getsubtototal();
        }
        function getsubtototal(){
            var sum = 0;
            $(".getsubtotal").each(function(){
                var value = $(this).text();
                sum += parseFloat(value);
            });
            $('#subtotal').val(sum);
        }
        function verifysubmit(trid){
            if($('#subtotal').val() != 0){
                if($('#methoding').val() == 1){
                    if($('#accounts').val() != "") {
                        submittotransact(trid);
                    }else{
                        alert("Please Select Customer");
                    }
                }else{
                    submittotransact(trid);
                }
            }else{
                alert("Please Add Product from List");
            }

        }

        function submittotransact(trid){
            if(confirm('Are you sure you want to make this transaction?')){
                $("#"+trid).each(function(){
                    $(this).find('tr').each (function() {
                        var prod = $(this).find('td:eq(0)').text();
                        var qty=$(this).find('td:eq(2)').text();
                        var total=$(this).find('td:eq(3)').text();
                        var method = $('#methoding').val();
                        var accounts = $('#accounts').val();
                        var cashtot = $('#subtotal').val();
                        var receipt = 'N/A';
                        $('#submitbtn').hide();
                        if($('#receipt').val() == ''){
                            receipt = 'N/A';
                        }
                        else{
                            receipt = $('#receipt').val();
                        }
                        if(!(prod.length === 0)){
                        }
                        $.ajax({
                            url: '/sales_transaction/sold',
                            method: 'POST',
                            data: {
                                _token: $('#toktoken input[name=_token]').val(),
                                product: prod,
                                quantity: qty,
                                total: total,
                                account: accounts,
                                cash: cashtot,
                                receipt: receipt,
                                payment: method
                            },
                            dataType: 'json',
                            success: function(data){
                                alert(data.message);
                            },
                            error: function (data) {
                                console.log(data);
                            },
                            complete: function(){
//                            $('#sales_inventory_table').dataTable().fnDraw();
                            }
                        });
                    });
                });
            }else{
                alert('Canceled');
            }
        }


    </script>
@endsection
