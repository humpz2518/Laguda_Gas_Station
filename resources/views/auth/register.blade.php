@extends('layouts.landing-layout')

@section('content')
    <!-- Top content -->
    <div class="top-content">

        <div class="inner-bg">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2 text">
                        <h1><strong>Welcome to Laguda's Gasoline Station</strong></h1>
                        <div class="description">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3 form-box">
                        <div class="form-top">
                            <div class="form-top-left">
                                <h3>Register</h3>
                                <p>Enter your details to register</p>
                            </div>
                            <div class="form-top-right">
                                <i class="glyphicon glyphicon-user"></i>
                            </div>
                        </div>
                        <div class="form-bottom">
                            <form class="login-form" method="POST" action="{{ route('register') }}">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <label class="sr-only" for="name">First Name</label>
                                    <input type="text" name="name" placeholder="Enter Name" class="form-username form-control" id="name" required autofocus>
                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label class="sr-only" for="email">Email</label>
                                    <input type="email" name="email" placeholder="Enter Email" class="form-username form-control" id="email" required>
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label class="sr-only" for="password">Password</label>
                                    <input type="password" name="password" placeholder="Enter Password" class="form-password form-control" id="password" required>
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label class="sr-only" for="password_confirmation">Confirm Password</label>
                                    <input type="password" name="password_confirmation" placeholder="Confirm Password" class="form-password form-control" id="password_confirmation" required>
                                </div>
                                <button type="submit" class="btn" style="float: left; background: green;">Register</button>
                                <a href="{{route('welcome')}}"><button type="button" class="btn" style="float: right;">Cancel</button></a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop