<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
})->name('welcome');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/accounts_receivable', 'HomeController@accounts_receivable')->name('accounts_receivable');
Route::get('/reports/gasoline', 'HomeController@gasoline_reports')->name('gasoline_reports');
Route::get('/reports/sales', 'HomeController@sales_reports')->name('sales_reports');
Route::get('/inventory/gasoline', 'HomeController@gasoline_inventory')->name('gasoline_inventory');
Route::get('/inventory/sales', 'HomeController@sales_inventory')->name('sales_inventory');
Route::get('/gas_transaction', 'HomeController@gas_transaction')->name('gas_transaction');
Route::get('/sales_transaction', 'HomeController@sales_transaction')->name('sales_transaction');
Route::get('/logs', 'HomeController@logs')->name('logs');
Route::post('/inventory/gasoline/save', 'GasolineController@save_gasoline_price')->name('gasoline_save_price');
Route::post('/inventory/gasoline/add', 'GasolineController@add_gasoline_litres')->name('gasoline_add_litres');
Route::post('/gas_transaction/get_litres', 'GasolineController@get_litres')->name('gasoline_get_litres');
Route::post('/accounts_receivable/add_customer', 'GasolineController@add_customer')->name('add_customer');
Route::post('/gas_transaction/save_transaction', 'GasolineController@save_transaction')->name('save_transaction');

//Datatable
Route::get('/gasoline_inventory/datatable','DataTableController@View_Gasoline_Inventory_Datatable')->name('gasoline-invetory-value');
Route::get('/sales_inventory/datatable','DataTableController@View_Sales_Inventory_Datatable')->name('sales-inventory-value');
Route::get('/sales_inventory/datatable/logs','DataTableController@View_Sales_Transaction_Datatable')->name('sales-transaction-value');
Route::get('/sales_transaction/datatable','DataTableController@View_Sales_Transaction_Buy_Datatable')->name('sales-transaction-buy-value');
Route::get('/sales_report/datatable','DataTableController@View_Sales_Report_Datatable')->name('sales-report');
Route::get('/gasoline_report/datatable','DataTableController@View_Gasoline_Report_Datatable')->name('gasoline-report');
Route::get('/gasoline_transactions/datatable','DataTableController@View_Gasoline_Transactions_Datatable')->name('gasoline-transactions-value');
Route::get('/logs/datatable','DataTableController@View_Logs_Datatable')->name('logs-value');
Route::get('/accounts_receivable/datatable','DataTableController@Accounts_Receivable_Datatable')->name('accounts-receivable-value');

//CSV
Route::get('/gasoline_inventory/csv','CSVController@gasoline_inventory')->name('gasoline-inventory-csv');
Route::get('/gasoline_reports/csv','CSVController@gasoline_reports')->name('gasoline-reports-csv');
Route::get('/sales_inventory/csv','CSVController@sales_inventory')->name('sales-inventory-csv');
Route::get('/sales_reports/csv','CSVController@sales_reports')->name('sales-reports-csv');
Route::get('/accounts_receivable/csv','CSVController@accounts_receivable')->name('accounts-receivable-csv');


Route::post('/inventory/sales/add', array('uses'=>'SalesController@sales_add_item'))->name('sales_add_item');
Route::post('/inventory/sales/save', 'SalesController@save_sales_price')->name('sales_save_price');
Route::post('/inventory/sales/add_quantity', array('uses'=>'SalesController@sales_add_quantity'))->name('sales_add_quantity');
Route::post('/sales_transaction/sold', array('uses'=>'SalesController@sales_sold'))->name('sales_sold');